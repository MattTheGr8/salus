//
//  SALInfoViewController.m
//  Salus
//
//  Created by Matthew Johnson on 4/22/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import "SALInfoViewController.h"

@implementation SALInfoViewController

- (id) init         //designated initializer
{
    self = [super init];
    if (self)
    {
        CGRect appFrame = [[UIScreen mainScreen] applicationFrame];
        UIView *mainView = [[UIView alloc] initWithFrame:CGRectZero];
        mainView.backgroundColor = [UIColor whiteColor];
        self.view = mainView;
        self.imView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"info_tab.png"]];
        CGRect imFrame = self.imView.frame;
        imFrame.origin.x = appFrame.origin.x;
        imFrame.origin.y = appFrame.origin.y;
        self.imView.frame = imFrame;
        [self.view addSubview:self.imView];
    }
    return self;
}


@end
