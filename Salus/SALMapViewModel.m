//
//  SALMapViewModel.m
//  Salus
//
//  Created by Matthew Johnson on 6/29/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import "SALMapViewModel.h"
#import "SALMapViewController.h"
#import "SALMapViewAnnotation.h"
#import "SALLocationFinder.h"

@implementation SALMapViewModel

- (id) initWithController:(SALMapViewController *) controller         //designated initializer
{
    self = [super init];
    if (self)
    {
        self.viewController = controller;
        self.annotations = nil;
        //probably more later...?
    }
    return self;
}

- (void) updateModelWithCoord:(CLLocationCoordinate2D) newCoord andSpan:(MKCoordinateSpan) newSpan force:(BOOL)doForce
{
    self.coord = newCoord;
    self.mapSpan = newSpan;
    
    if (doForce || self.annotations == nil || [self haveAnnotationsForNewSpan] == NO)
        [self requestNewAnnotationsFromServer];
    else
        [self sendAnnotationsToViewController];
}

- (BOOL) haveAnnotationsForNewSpan
{
    if (self.annotations == nil)
        return NO;
    
    // find largest distance from request origin of all current annotations
    CGFloat maxDist = 0;
    SALMapViewAnnotation *annotTemp;
    CLLocationDistance distTemp;
    for (NSUInteger i = 0; i < self.annotations.count; i++)
    {
        annotTemp = [self.annotations objectAtIndex:i];
        distTemp = [annotTemp distanceFromRequestOrigin:self.lastAnnotationRequestCoord];
        if (distTemp > maxDist)
            maxDist = distTemp;
    }
    
    // define corners of new span
    CLLocationDegrees westLong = self.coord.longitude - (self.mapSpan.longitudeDelta / 2);
    CLLocationDegrees eastLong = self.coord.longitude + (self.mapSpan.longitudeDelta / 2);
    CLLocationDegrees southLat = self.coord.latitude  - (self.mapSpan.latitudeDelta  / 2);
    CLLocationDegrees northLat = self.coord.latitude  + (self.mapSpan.latitudeDelta  / 2);
    
    // create CLLocations for the center of the request origin and the corners of the new span
    CLLocation *reqOrigin = [[CLLocation alloc] initWithLatitude:self.lastAnnotationRequestCoord.latitude longitude:self.lastAnnotationRequestCoord.longitude];
    //CLLocation *tempLoc;
    
    // test each corner of the new span
//    tempLoc = [[CLLocation alloc] initWithLatitude:northLat longitude:westLong];
//    if ([reqOrigin distanceFromLocation:tempLoc] > maxDist)
//        return NO;
//    tempLoc = [[CLLocation alloc] initWithLatitude:northLat longitude:eastLong];
//    if ([reqOrigin distanceFromLocation:tempLoc] > maxDist)
//        return NO;
//    tempLoc = [[CLLocation alloc] initWithLatitude:southLat longitude:westLong];
//    if ([reqOrigin distanceFromLocation:tempLoc] > maxDist)
//        return NO;
//    tempLoc = [[CLLocation alloc] initWithLatitude:southLat longitude:eastLong];
//    if ([reqOrigin distanceFromLocation:tempLoc] > maxDist)
//        return NO;
    if ([SALLocationFinder distanceFromCLLocation:reqOrigin toLat:northLat andLong:westLong] > maxDist)
        return NO;
    if ([SALLocationFinder distanceFromCLLocation:reqOrigin toLat:northLat andLong:eastLong] > maxDist)
        return NO;
    if ([SALLocationFinder distanceFromCLLocation:reqOrigin toLat:southLat andLong:westLong] > maxDist)
        return NO;
    if ([SALLocationFinder distanceFromCLLocation:reqOrigin toLat:southLat andLong:eastLong] > maxDist)
        return NO;
    
    // if none of the corners of the new span are outside the old request radius...
    return YES;
}

- (void) requestNewAnnotationsFromServer
{
    //NSLog(@"Requesting new annotations...");
    
    // create URL request string based on current latitude/longitude
    NSURL *url = [[NSURL alloc] initWithString:kSALMapAnnotationsJSON_URL];
    NSMutableURLRequest *urlReq = [[NSMutableURLRequest alloc] initWithURL:url];
    NSString *postString = [NSString stringWithFormat:@"latitude=%f&longitude=%f", self.coord.latitude, self.coord.longitude];
    
    [urlReq setHTTPMethod:@"POST"];
    [urlReq setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    // call a function to parse returned JSON into annotation objects and call that from our completion handler
    //  if that is successful, send the received annotations back to the view controller
    [NSURLConnection sendAsynchronousRequest:urlReq queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
             // at some future point:
             // 1. add fallback local database that gets updated when the app does in the App Store
             // 2. add a callback hook to keep trying
             NSLog(@"Failed to load map annotations URL");
         else {
             [self parseJSONIntoAnnotationsArray:data];
             [self sendAnnotationsToViewController];
         }
     }];
}

- (void) sendAnnotationsToViewController
{
    [self.viewController addModelAnnotationsToMapView];
}

- (void) parseJSONIntoAnnotationsArray:(NSData *)jsonData
{
    //NSString* tempStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //NSLog(@"Here is the JSON data we got: %@\n", tempStr);
    
    NSError *parseError = nil;
    NSArray *allDataArray = nil;
    id arrayTemp = nil;
    arrayTemp = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&parseError];
    if (arrayTemp == nil || parseError != nil) //if there's an error, both should be true, but whatever
    {
        NSLog(@"JSON parsing error!"); // later: add in better error handling?
        // do anything else here to clean up? or retry request?
        return;
    }
    
    if ( ![arrayTemp isKindOfClass:[NSArray class] ] )
    {
        NSLog(@"Map API did not return an array, as expected...");
        // additional cleanup or retry? (as above)
        return;
    }
    
    //if we've got this far, let's assign the data to an NSArray pointer so we can easily treat it as such
    allDataArray = arrayTemp;
    id annotDictTemp;
    SALMapViewAnnotation *newAnnot;
    NSMutableArray *newAnnotations = [[NSMutableArray alloc] init];
    for ( NSUInteger i = 0; i < [allDataArray count]; i++ )
    {
        annotDictTemp = [allDataArray objectAtIndex:i];
        if (![annotDictTemp isKindOfClass:[NSDictionary class]])
        {
            NSLog(@"An object within the array returned by the Map API was not a dictionary, as expected...");
            // additional cleanup or retry? (as above)
            continue;
        }
        newAnnot = [SALMapViewAnnotation mapViewAnnotationFromDictionary:annotDictTemp];
        if (newAnnot == nil)
            NSLog(@"Dictionary could not be converted to new map annotation object..."); // again, handle more/better later
        else
            [newAnnotations addObject:newAnnot];
    }
    
    // if everything was successful, update annotations and lastAnnotationRequestCoord
    //[self.viewController clearMapViewAnnotations];
    [self.viewController performSelectorOnMainThread:@selector(clearMapViewAnnotations) withObject:nil waitUntilDone:YES];
    self.annotations = newAnnotations;
    self.lastAnnotationRequestCoord = self.coord;
}



@end
