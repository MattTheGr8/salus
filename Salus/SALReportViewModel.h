//
//  SALReportViewModel.h
//  Salus
//
//  Created by Matthew Johnson on 8/28/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import <Foundation/Foundation.h>
@class SALReportViewModelScreen;

@interface SALReportViewModel : NSObject

    @property (strong) SALReportViewModelScreen *rootReportScreen;

@end
