//
//  SALAppDelegate.h
//  Salus
//
//  Created by Matthew Johnson on 4/17/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SALLocationFinder;
@class SALMapViewController;
@class SALNetworkImageCache;

@interface SALAppDelegate : UIResponder <UIApplicationDelegate>

    @property (strong, nonatomic) UIWindow *window;
    @property (strong) UITabBarController *tabBarController;
    @property (strong) SALLocationFinder *locFinder;
    @property (assign) BOOL inited;
    @property (strong) SALNetworkImageCache *globalImageCache;

    // view controllers for various tabs -- fill in as needed when/if controllers need to talk to each other?
    @property (strong) SALMapViewController *mapVC;
@end
