//
//  SALNetworkImageCache.h
//  Salus
//
//  Created by Matthew Johnson on 10/12/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SALNetworkImageCache : NSObject

    @property (strong) NSMutableDictionary *urlImgDict;

    + (UIImage *) globalCacheGetImageForURL:(NSString *)url;
    + (void) globalCacheSetImage:(UIImage *)img forURL:(NSString *)url;
    + (BOOL) globalCacheAlreadyHas:(NSString *)url;

@end
