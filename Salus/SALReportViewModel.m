//
//  SALReportViewModel.m
//  Salus
//
//  Created by Matthew Johnson on 8/28/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import "SALReportViewModel.h"
#import "SALReportViewModelScreen.h"

@implementation SALReportViewModel

- (SALReportViewModel *) init       //designated initializer
{
    // at some point, will probably want to load this from a p-list or the web or something... for now, will hard-code
    
    SALReportViewModelScreen *specIllScreen = [[SALReportViewModelScreen alloc] init];
    specIllScreen.title = @"Specific illness";
    specIllScreen.childrenTitles = [NSArray arrayWithObjects:@"Cholera",@"Malaria",@"Whooping Cough",@"Zombieism", nil];
    specIllScreen.children = [NSArray arrayWithObjects:@"cholera",@"malaria",@"whooping_cough",@"zombieism", nil];
    
    SALReportViewModelScreen *symptomScreen = [[SALReportViewModelScreen alloc] init];
    symptomScreen.title = @"Symptom";
    symptomScreen.childrenTitles = [NSArray arrayWithObjects:@"Fever",@"Loss of appetite",@"Nausea",@"Craving for flesh", nil];
    symptomScreen.children = [NSArray arrayWithObjects:@"fever",@"appetite_loss",@"nausea",@"flesh_craving", nil];
    
    SALReportViewModelScreen *weatherScreen = [[SALReportViewModelScreen alloc] init];
    weatherScreen.title = @"Severe weather";
    weatherScreen.childrenTitles = [NSArray arrayWithObjects:@"Earthquake",@"Fire",@"Major storm",@"Swarm of locusts", nil];
    weatherScreen.children = [NSArray arrayWithObjects:@"earthquake",@"fire",@"major_storm",@"locusts", nil];
    
    SALReportViewModelScreen *socPolitScreen = [[SALReportViewModelScreen alloc] init];
    socPolitScreen.title = @"Social/political risk";
    socPolitScreen.childrenTitles = [NSArray arrayWithObjects:@"Assault",@"Pickpocketing",@"Riot",@"Warfare", nil];
    socPolitScreen.children = [NSArray arrayWithObjects:@"assault",@"pickpocket",@"riot",@"warfare", nil];
    
    self.rootReportScreen = [[SALReportViewModelScreen alloc] init];
    self.rootReportScreen.title = @"Report...";
    self.rootReportScreen.childrenTitles = [NSArray arrayWithObjects:@"Specific illness",@"Symptom",@"Severe weather",@"Social/political risk", nil];
    self.rootReportScreen.children = [NSArray arrayWithObjects:specIllScreen,symptomScreen,weatherScreen,socPolitScreen, nil];
    
    specIllScreen.parent = self.rootReportScreen;
    symptomScreen.parent = self.rootReportScreen;
    weatherScreen.parent = self.rootReportScreen;
    socPolitScreen.parent = self.rootReportScreen;
    
    return self;
}


@end
