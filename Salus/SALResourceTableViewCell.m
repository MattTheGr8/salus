//
//  SALResourceTableViewCell.m
//  Salus
//
//  Created by Matthew Johnson on 10/4/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import "SALResourceTableViewCell.h"
#import "SALResourceViewModelItem.h"
#import "SALNetworkImageCache.h"

@implementation SALResourceTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (style != UITableViewCellStyleSubtitle)
        NSLog(@"Warning: SALResourceTableViewCell can only be used with style UITableViewCellStyleSubtitle for now. Defaulting to that.");
    
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    if (self) {
        [self resetThumbImage];
        self.rating = nil;
    }
    return self;
}

- (void) configureFromModelItem:(SALResourceViewModelItem *)item
{
    self.textLabel.text = item.name;
    self.detailTextLabel.text = [NSString stringWithFormat:@"%@ - %@ - %@",
                                 [item categoryStr],
                                 [item distanceStrLocalized],
                                 [item reviewCountStr] ];
    //[self addSubview:self.thumbnail];
    self.indentationLevel = 1;
    self.indentationWidth = kSALResourceTableViewCellThumbnailSize - 10 + kSALResourceTableViewCellThumbnailXOffset;
    [self loadThumbnailAsync:item.imageURL];
    [self loadRatingAsync:item.ratingImageURL];
    [self loadYelpLogoAsync];
    
    // presumably we'll always have a next view to display
    self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
}

- (void) layoutSubviews
{
    [super layoutSubviews];  //The default implementation of the layoutSubviews
    
    CGRect labelRect = self.textLabel.frame;
    labelRect.size.width = self.frame.size.width - labelRect.origin.x - kSALResourceTableViewCellRatingXOffset - kSALResourceTableViewCellRatingWidth;
    self.textLabel.frame = labelRect;
    
    labelRect = self.detailTextLabel.frame;
    labelRect.size.width = self.frame.size.width - labelRect.origin.x - kSALResourceTableViewCellRatingXOffset - kSALResourceTableViewCellRatingWidth;
    self.detailTextLabel.frame = labelRect;
    
    self.logo.frame = self.logoRect;
    self.rating.frame = self.ratingRect;
}

- (void) resetThumbImage
{
    UIImage *thumbImage = [UIImage imageNamed:kSALResourceTableViewCellDefaultImageName];
    [self doThumbnailWithImage:thumbImage];
}

- (void) loadThumbnailAsync:(NSString *)thumbURL
{
    if (thumbURL == nil || [thumbURL isEqualToString:@""])
        return;
    
    // create URL request
    NSURL *url = [[NSURL alloc] initWithString:thumbURL];
    NSURLRequest *urlReq = [[NSURLRequest alloc] initWithURL:url];
    
    // call a function to parse returned JSON into annotation objects and call that from our completion handler
    //  if that is successful, send the received annotations back to the view controller
    [NSURLConnection sendAsynchronousRequest:urlReq queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
             // at some future point:
             // 1. add fallback local database that gets updated when the app does in the App Store
             // 2. add a callback hook to keep trying
             NSLog(@"Failed to load thumbnail image URL");
         else {
             UIImage *newThumbImage = [UIImage imageWithData:data];
             // might have to do on main thread?
             //NSLog(@"Doing new thumbnail");
             
             [self performSelectorOnMainThread:@selector(doThumbnailWithImage:) withObject:newThumbImage waitUntilDone:NO];
             //[self doThumbnailWithImage:newThumbImage];
             
         }
     }];
}


- (void) doThumbnailWithImage:(UIImage *)img
{
    if (self.thumbnail != nil)
        [self.thumbnail removeFromSuperview];
    
    self.thumbnail = [[UIImageView alloc] initWithImage:img];
    self.thumbnail.frame = [self rectForThumbnail];
    [self addSubview:self.thumbnail];
}


- (void) loadRatingAsync:(NSString *)ratingURL
{
    if (ratingURL == nil || [ratingURL isEqualToString:@""])
        return;
    
    if ([SALNetworkImageCache globalCacheAlreadyHas:ratingURL])
    {
        [self doRatingWithImage:[SALNetworkImageCache globalCacheGetImageForURL:ratingURL]];
        //NSLog(@"Reusing image!");
        return;
    }
    
    // create URL request
    NSURL *url = [[NSURL alloc] initWithString:ratingURL];
    NSURLRequest *urlReq = [[NSURLRequest alloc] initWithURL:url];
    
    // call a function to parse returned JSON into annotation objects and call that from our completion handler
    //  if that is successful, send the received annotations back to the view controller
    [NSURLConnection sendAsynchronousRequest:urlReq queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
             // at some future point:
             // 1. add fallback local database that gets updated when the app does in the App Store
             // 2. add a callback hook to keep trying
             NSLog(@"Failed to load rating image URL");
         else {
             UIImage *ratingImage = [UIImage imageWithData:data];
             // might have to do on main thread?
             //NSLog(@"Doing new thumbnail");
             
             [self performSelectorOnMainThread:@selector(doRatingWithImage:) withObject:ratingImage waitUntilDone:NO];
             [SALNetworkImageCache globalCacheSetImage:ratingImage forURL:ratingURL];
         }
     }];
}


- (void) doRatingWithImage:(UIImage *)img
{
    if (self.rating != nil)
        [self.rating removeFromSuperview];
    
    self.rating = [[UIImageView alloc] initWithImage:img];
    self.rating.frame = [self rectForRatingImage:img];
    [self addSubview:self.rating];
}


- (void) loadYelpLogoAsync
{
    if ([SALNetworkImageCache globalCacheAlreadyHas:kSALResourceTableViewCellYelpLogoURL])
    {
        [self doYelpLogoWithImage:[SALNetworkImageCache globalCacheGetImageForURL:kSALResourceTableViewCellYelpLogoURL]];
        //NSLog(@"Reusing image!");
        return;
    }
    
    // create URL request
    NSURL *url = [[NSURL alloc] initWithString:kSALResourceTableViewCellYelpLogoURL];
    NSURLRequest *urlReq = [[NSURLRequest alloc] initWithURL:url];
    
    // call a function to parse returned JSON into annotation objects and call that from our completion handler
    //  if that is successful, send the received annotations back to the view controller
    [NSURLConnection sendAsynchronousRequest:urlReq queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
             // at some future point:
             // 1. add fallback local database that gets updated when the app does in the App Store
             // 2. add a callback hook to keep trying
             NSLog(@"Failed to load Yelp logo image URL");
         else {
             UIImage *logoImage = [UIImage imageWithData:data];
             // might have to do on main thread?
             //NSLog(@"Doing new thumbnail");
             
             [self performSelectorOnMainThread:@selector(doYelpLogoWithImage:) withObject:logoImage waitUntilDone:NO];
             [SALNetworkImageCache globalCacheSetImage:logoImage forURL:kSALResourceTableViewCellYelpLogoURL];
         }
     }];
    
}


- (void) doYelpLogoWithImage:(UIImage *)img
{
    if (self.logo != nil)
        [self.logo removeFromSuperview];
    
    self.logo = [[UIImageView alloc] initWithImage:img];
    self.logo.frame = [self rectForLogoImage:img];
    [self addSubview:self.logo];
}


- (CGRect) rectForThumbnail
{
    return CGRectMake(kSALResourceTableViewCellThumbnailXOffset, kSALResourceTableViewCellThumbnailYOffset, kSALResourceTableViewCellThumbnailSize, kSALResourceTableViewCellThumbnailSize);
}


- (CGRect) rectForRatingImage:(UIImage *)img
{
    CGFloat x = self.frame.size.width - kSALResourceTableViewCellRatingXOffset - kSALResourceTableViewCellRatingWidth;
    CGFloat ht = img.size.height / img.size.width * kSALResourceTableViewCellRatingWidth;
    self.ratingRect = CGRectMake(x, kSALResourceTableViewCellRatingYOffset, kSALResourceTableViewCellRatingWidth, ht);
    return self.ratingRect;
}


- (CGRect) rectForLogoImage:(UIImage *)img;
{
    CGFloat x = self.frame.size.width - kSALResourceTableViewCellRatingXOffset - kSALResourceTableViewCellRatingWidth;
    CGFloat ht = img.size.height / img.size.width * kSALResourceTableViewCellRatingWidth;
    self.logoRect = CGRectMake(x, kSALResourceTableViewCellYelpLogoYOffset, kSALResourceTableViewCellRatingWidth, ht);
    return self.logoRect;
}



/*- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}*/

@end
