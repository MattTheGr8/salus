//
//  SALLocationFinder.h
//  Salus
//
//  Created by Matthew Johnson on 6/8/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
@class SALEmergencyViewModel;
@class SALMapViewController;
@class SALResourceViewModel;

#define kSALLocationFinderRetryTime 30
#define kSALLocationFinderUpdateRadiusMeters 200
#define kSALLocationFinderForceLocationUpdateTime 60 // debug; probably set back to 600  //in seconds; 600 = 10 minutes
#define kSALLocationFinderLocationPermissionDeniedMessage @"Salus needs to know your current location in order to provide information to you.\n\nIt will only get location updates while the app is in use.\n\nPlease go into your device settings and enable location services for Salus to be able to work."
#define kSALLocationFinderLocationServicesTurnedOffMessage @"Salus needs to know your current location in order to provide information to you.\n\nCurrently, it looks like location services are disabled/unavailable on your device.\n\nPlease go into your device settings and enable location services for Salus to be able to work."

#define kSALLocationFinderAddressSearchRadiusMultiplier 2 // if we think the address is 1km away, this will search up to 2km away

@interface SALLocationFinder : NSObject <CLLocationManagerDelegate>
    @property (strong) SALEmergencyViewModel *emergencyModel;
    @property (strong) SALResourceViewModel *resourceModel;
    @property (strong) SALMapViewController *mapVC;
    @property (strong) CLLocationManager *locationManager;
    @property (strong) CLGeocoder *geocoder;
    @property (strong) NSTimer *forceUpdateTimer;

    @property (strong) CLLocation *currentLocation;
    @property (strong) CLPlacemark *currentLocPlacemark;
//    @property (strong) CLCircularRegion *currentUpdateRegion;

    @property (assign) BOOL haveInitialFix;
    @property (assign) BOOL doingHighEnergyUpdating;

    - (void)updateEverybody;
    - (void)updateEmergencyModel;
    - (void)updateResourceModel;
    - (void)updateMapVC;
    - (void)checkAuthorizationAndInitStuff;
    - (void)startUpdating;
    - (void)startLeisurelyUpdating;
    - (void)resetUpdateRegion;
    - (void)stopRegionMonitoring;
    - (void)shutDown;
    - (void)forceLocationUpdate;
    - (void)resetLocationUpdateTimer;
    - (void)displayLocationPermissionRequest;
    - (void)displayLocationTurnOnRequest;

    //CLLocationManagerDelegate methods
    - (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations;
    - (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error;
    - (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error;
    - (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region;

    // convenience methods
    + (CLLocationDistance) distanceFromCLLocation:(CLLocation *)clloc toLat:(CLLocationDegrees)latitude andLong:(CLLocationDegrees)longitude;
    + (CLLocationDistance) distanceBetweenCLLocCoord:(CLLocationCoordinate2D)coord1 andCLLocCoord:(CLLocationCoordinate2D)coord2;
    + (void) findMostLikelyCoordForAddress:(NSString *)address andCoord:(CLLocationCoordinate2D)coord withDistance:(CLLocationDistance)meters callbackObj:(id)obj andSelector:(SEL)sel;;
    + (void) findMostLikelyCoordForAddressStrArray:(NSArray *)addressStrs andCoord:(CLLocationCoordinate2D)coord withDistance:(CLLocationDistance)meters callbackObj:(id)obj andSelector:(SEL)sel;
    + (CLPlacemark *) findBestMatchPlacemarkToCoord:(CLLocationCoordinate2D)coord fromPlacemarks:(NSArray *)placemarks withDistance:(CLLocationDistance)meters;

    // boatload to do one simple thing in Resource sub-view...
    + (CLLocationCoordinate2D) midpointBetweenCoord:(CLLocationCoordinate2D)coord1 andCoord:(CLLocationCoordinate2D)coord2;
    + (CLLocationDistance) latDistanceMetersBetweenCoord:(CLLocationCoordinate2D)coord1 andCoord:(CLLocationCoordinate2D)coord2;
    + (CLLocationDistance) longDistanceMetersBetweenCoord:(CLLocationCoordinate2D)coord1 andCoord:(CLLocationCoordinate2D)coord2;
    + (CLLocationCoordinate2D) minLatLongOfCoord:(CLLocationCoordinate2D)coord1 andCoord:(CLLocationCoordinate2D)coord2;
    + (CLLocationCoordinate2D) maxLatLongOfCoord:(CLLocationCoordinate2D)coord1 andCoord:(CLLocationCoordinate2D)coord2;
@end
