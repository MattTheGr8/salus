//
//  SALMapViewAnnotation.m
//  Salus
//
//  Created by Matthew Johnson on 7/11/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import "SALMapViewAnnotation.h"
#import "SALMapViewModel.h"
#import "SALLocationFinder.h"

@implementation SALMapViewAnnotation


+ (SALMapViewAnnotation *) mapViewAnnotationFromDictionary:(NSDictionary *)dict
{
    // format coming in should be a dictionary with keys:
    //  event_type: (string) (kSALMapViewAnnotationEventTypeKey)
    //  latitude: (float) (kSALMapViewAnnotationLatitudeKey)
    //  longitude: (float) (kSALMapViewAnnotationLongitudeKey)
    //  timestamp: (long int, UNIX epoch time) (kSALMapViewAnnotationTimestampKey)
    //  other: (dict) (kSALMapViewAnnotationOtherKey)
    //  - keys vary depending on event type
    
    id tempVal;
    
    // unpack event type
    tempVal = [dict objectForKey:kSALMapViewAnnotationEventTypeKey];
    if (tempVal == nil || ![tempVal isKindOfClass:[NSString class]])
    {
        NSLog(@"Unable to parse event type from JSON!");
        return nil;
    }
    NSString *eventTypeStr = tempVal;
    NSUInteger eventTypeInt = [SALMapViewAnnotation eventTypeCodeForServerEventTypeString:eventTypeStr];
    
    // unpack latitude and longitude
    tempVal = [dict objectForKey:kSALMapViewAnnotationLatitudeKey];
    if (tempVal == nil || ![tempVal isKindOfClass:[NSNumber class]])
    {
        NSLog(@"Unable to parse latitude from JSON!");
        return nil;
    }
    NSNumber *tempNumber = tempVal;
    CLLocationDegrees latitude = [tempNumber doubleValue];
    
    tempVal = [dict objectForKey:kSALMapViewAnnotationLongitudeKey];
    if (tempVal == nil || ![tempVal isKindOfClass:[NSNumber class]])
    {
        NSLog(@"Unable to parse longitude from JSON!");
        return nil;
    }
    tempNumber = tempVal;
    CLLocationDegrees longitude = [tempNumber doubleValue];
    CLLocationCoordinate2D annotCoord = CLLocationCoordinate2DMake(latitude, longitude);
    
    // unpack timestamp
    tempVal = [dict objectForKey:kSALMapViewAnnotationTimestampKey];
    if (tempVal == nil || ![tempVal isKindOfClass:[NSNumber class]])
    {
        NSLog(@"Unable to parse timestamp from JSON!");
        return nil;
    }
    tempNumber = tempVal;
    NSTimeInterval timestamp = [tempNumber doubleValue];
    
    // unpack "other" dictionary
    tempVal = [dict objectForKey:kSALMapViewAnnotationOtherKey];
    if (tempVal == nil || ![tempVal isKindOfClass:[NSDictionary class]])
    {
        NSLog(@"Unable to parse 'other' field from JSON!");
        return nil;
    }
    NSDictionary *otherDict = [NSDictionary dictionaryWithDictionary:tempVal];
    // for the moment, won't bother about unpacking "other" dictionaries further... just make a dictionary of them and let the actual
    //  annotation subtypes sort it out
    
    // make annotation object and return
    SALMapViewAnnotation *newAnnot = [[SALMapViewAnnotation alloc] init];
    newAnnot.coordinate = annotCoord;
    newAnnot.isPoint = YES; // this will definitely have to change once we implement non-point annotations
    newAnnot.eventType = eventTypeInt;
    newAnnot.timestamp = timestamp;
    newAnnot.other = otherDict;
    
    return newAnnot;
}

+ (NSString *) eventNameForEventType:(NSUInteger)type
{
    // need a much more robust and localizable implementation later...
    switch (type)
    {
        case kSALMapViewAnnotationTypeFire:
            return kSALMapViewAnnotationTypeFireStr;
        case kSALMapViewAnnotationTypeEarthquake:
            return kSALMapViewAnnotationTypeEarthquakeStr;
        case kSALMapViewAnnotationTypeStormCell:
            return kSALMapViewAnnotationTypeStormCellStr;
        case kSALMapViewAnnotationTypeStormReport:
            return kSALMapViewAnnotationTypeStormReportStr;
        case kSALMapViewAnnotationTypeZombies:
            return kSALMapViewAnnotationTypeZombieStr;
        default:
            return @"";
    }
}

+ (NSUInteger) eventTypeCodeForServerEventTypeString:(NSString *)typeStr
{
    if ([typeStr isEqualToString:kSALMapViewAnnotationTypeFireServerStr])
        return kSALMapViewAnnotationTypeFire;
    else if ([typeStr isEqualToString:kSALMapViewAnnotationTypeEarthquakeServerStr])
        return kSALMapViewAnnotationTypeEarthquake;
    else if ([typeStr isEqualToString:kSALMapViewAnnotationTypeStormCellServerStr])
        return kSALMapViewAnnotationTypeStormCell;
    else if ([typeStr isEqualToString:kSALMapViewAnnotationTypeStormReportServerStr])
        return kSALMapViewAnnotationTypeStormReport;
    else if ([typeStr isEqualToString:kSALMapViewAnnotationTypeZombieServerStr])
        return kSALMapViewAnnotationTypeZombies;
    else
        return 0;
}

+ (NSString *) eventServerStrForEventType:(NSUInteger)type
{
    // need a much more robust and localizable implementation later...
    switch (type)
    {
        case kSALMapViewAnnotationTypeFire:
            return kSALMapViewAnnotationTypeFireServerStr;
        case kSALMapViewAnnotationTypeEarthquake:
            return kSALMapViewAnnotationTypeEarthquakeServerStr;
        case kSALMapViewAnnotationTypeStormCell:
            return kSALMapViewAnnotationTypeStormCellServerStr;
        case kSALMapViewAnnotationTypeStormReport:
            return kSALMapViewAnnotationTypeStormReportServerStr;
        case kSALMapViewAnnotationTypeZombies:
            return kSALMapViewAnnotationTypeZombieServerStr;
        default:
            return @"";
    }
}

- (CLLocationDistance) distanceFromRequestOrigin:(CLLocationCoordinate2D)ogin
{
    return [SALLocationFinder distanceBetweenCLLocCoord:self.coordinate andCLLocCoord:ogin];
}

- (NSString *) eventTypeStr
{
    return [SALMapViewAnnotation eventNameForEventType:self.eventType];
}


- (NSString *) eventTypeServerStr
{
    return [SALMapViewAnnotation eventServerStrForEventType:self.eventType];
}



@end
