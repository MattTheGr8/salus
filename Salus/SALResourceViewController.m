//
//  SALResourceViewController.m
//  Salus
//
//  Created by Matthew Johnson on 9/22/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import "SALResourceViewController.h"
#import "SALResourceViewModel.h"
#import "SALResourceTableViewController.h"
#import "SALResourceSubViewController.h"

@interface SALResourceViewController ()

@end

@implementation SALResourceViewController

- (id) init                         //designated initializer
{
    SALResourceViewModel *model = [[SALResourceViewModel alloc] init];
    SALResourceTableViewController *tableVC = [[SALResourceTableViewController alloc] initWithStyle:UITableViewStylePlain andModel:model];
    
    self = [super initWithRootViewController:tableVC];
    if (self)
    {
        model.viewController = self;
        self.model = model;
        self.tableVC = tableVC;
        self.delegate = self;
        self.currentVC = tableVC;
        
        if (self.model.resourceList != nil)
            //[self updateWithCoordinate:self.model.coord];
            [self updateFromModel];
    }
    return self;
}


- (void) safelyPushViewController:(UIViewController *)vc
{
    if (self.safeToPushNewViewController)
    {
        self.safeToPushNewViewController = NO;
        [self pushViewController:vc animated:YES];
        return;
    }
    
    [self performSelector:@selector(safelyPushViewController:) withObject:vc afterDelay:0.5];
}

- (void) createPushSubViewControllerForModelItem:(SALResourceViewModelItem *)modelItem withImage:(UIImage *)img
{
    self.subVC = [[SALResourceSubViewController alloc] initWithResource:modelItem andImage:img];
    [self safelyPushViewController:self.subVC];
    
    // maybe that's all we need for now? check back later
}

//- (void) updateWithCoordinate:(CLLocationCoordinate2D)coord
//{
//    if (self.subVC != nil)
//        [self.subVC updateWithCoordinate:coord];
//    
//    [self.tableVC updateTableView];
//}

- (void) updateFromModel
{
    if (self.subVC != nil)
        [self.subVC updateWithCoordinate:self.model.coord];
    
    [self.tableVC updateTableView];
}


#pragma mark - Navigation controller delegate methods

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    self.safeToPushNewViewController = YES;
    self.currentVC = viewController;
    
    if (viewController == self.subVC)
        return;
    
    if (viewController == self.tableVC && self.subVC != nil)
        self.subVC = nil;
}





@end
