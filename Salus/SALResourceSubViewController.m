//
//  SALResourceSubViewController.m
//  Salus
//
//  Created by Matthew Johnson on 10/2/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import "SALResourceSubViewController.h"
#import "SALResourceViewModelItem.h"
#import "SALAppDelegate.h"
#import "SALUtilFuncs.h"
#import "SALResourceTableViewCell.h"
#import "SALNetworkImageCache.h"
#import "SALLocationFinder.h"
#import "SALCopyableUILabel.h"

@interface SALResourceSubViewController ()

@end

@implementation SALResourceSubViewController


- (id) initWithResource:(SALResourceViewModelItem *)resource andImage:(UIImage *)img
{
    self = [super init];
    if (self)
    {
        self.resource = resource;
        self.mapLink = nil; // just to be sure
        self.locationPlacemark = nil; // just to be sure
        
        self.locationCoord = CLLocationCoordinate2DMake(0, 0); // assume we don't find anything good off the coast of Ghana...
        //self.navigationItem.title = resource.name;
        self.navigationItem.title = @"Contact details";
        
        CGRect frame = self.view.frame;
        SALAppDelegate *appDel = [UIApplication sharedApplication].delegate;
        frame.origin.y += self.navigationController.navigationBar.frame.size.height;
        frame.size.height -= (self.navigationController.navigationBar.frame.size.height + appDel.tabBarController.tabBar.frame.size.height);
        self.sv = [[UIScrollView alloc] initWithFrame:frame];
        self.sv.autoresizesSubviews = NO;
        self.cv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 2000)];
        [self.sv addSubview:self.cv];
        self.sv.contentSize = CGSizeMake(frame.size.width, 2000);
        self.view = self.sv;
        [self makeConfigureYelpUpperRightView];
        [self makeConfigureContactUpperLeftViewWithImage:img];
        [self makeConfigureMapView];
    }
    return self;
}




- (void) updateWithCoordinate:(CLLocationCoordinate2D)coord
{
    // add in later
    
    // remember main thread check?
}


- (void) updateContentViewHeight
{
    if (self.mapLink == nil)
        return;
    
    CGRect frame = self.cv.frame;
    CGRect mapLinkFrame = self.mapLink.frame;
    frame.size.height = mapLinkFrame.origin.y + mapLinkFrame.size.height + kSALResourceSubViewBottomMargin;
    self.cv.frame = frame;
    self.sv.contentSize = frame.size;
}


- (void) launchMapsApp
{
    if (self.locationPlacemark == nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Could not find location!"
                                                        message:@"Sorry, the system was not able to pinpoint this location to pass off to the Maps app.\n\nMaybe try a manual search?"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    MKPlacemark *mk = [[MKPlacemark alloc] initWithPlacemark:self.locationPlacemark];
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:mk];
    mapItem.name = self.resource.name;
    mapItem.phoneNumber = self.resource.phoneNumber;
    mapItem.url = [NSURL URLWithString:self.resource.yelpLink];
    [mapItem openInMapsWithLaunchOptions:nil];
}

- (void) launchYelp
{
    [SALUtilFuncs openYelpURL:self.resource.yelpLink];
}


#pragma mark - Convenience methods for upper right part of view

- (void) makeConfigureYelpUpperRightView
{
    CGFloat rightMargin = 10;
    CGFloat topMargin = 10;
    CGFloat width = 70;
    CGFloat placeholderHeight = 16; // something is still not 100% right here -- changing from 5 to 10 should not change text position but it does
                                    // aha! It's because we define a constraint based on it! ... I guess that's OK for now? Easier than resizing...
    
    CGSize svSize = self.cv.frame.size;
    CGFloat x = svSize.width - rightMargin - width;
    
    NSLayoutConstraint *lc;
    
    
    // create star image placeholder view
    self.starPlaceholder = [[UIView alloc] initWithFrame:CGRectMake(x, topMargin, width, placeholderHeight) ];
    self.starPlaceholder.translatesAutoresizingMaskIntoConstraints = NO;
    [self.cv addSubview:self.starPlaceholder];
    // configure size and position with constraints
    lc = [NSLayoutConstraint constraintWithItem:self.starPlaceholder attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.cv attribute:NSLayoutAttributeLeft multiplier:1.0 constant:x]; // have no idea why this does not work with NSLayoutAttributeRight
    [self.cv addConstraint:lc];
    lc = [NSLayoutConstraint constraintWithItem:self.starPlaceholder attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.cv attribute:NSLayoutAttributeTop multiplier:1.0 constant:topMargin];
    [self.cv addConstraint:lc];
    lc = [NSLayoutConstraint constraintWithItem:self.starPlaceholder attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:width];
    [self.cv addConstraint:lc];
    lc = [NSLayoutConstraint constraintWithItem:self.starPlaceholder attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:placeholderHeight];
    [self.cv addConstraint:lc];
    // load the actual image, if possible
    [SALUtilFuncs loadImageAsyncWithURL:self.resource.ratingImageURL andCallbackObj:self andSelector:@selector(loadStarsImage:)];
    
    
    // create # of reviews label
    CGFloat starsReviewLabelMargin = 3.0;
    UILabel *nReviews = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 20)];
    nReviews.translatesAutoresizingMaskIntoConstraints = NO;
    nReviews.text = self.resource.reviewCountStr;
    nReviews.font = [UIFont systemFontOfSize:12.0];
    [nReviews sizeToFit];
    [self.cv addSubview:nReviews];
    // configure position with constraints
    lc = [NSLayoutConstraint constraintWithItem:nReviews attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:nReviews.frame.size.width];
    [self.cv addConstraint:lc];
    lc = [NSLayoutConstraint constraintWithItem:nReviews attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:nReviews.frame.size.height];
    [self.cv addConstraint:lc];
    lc = [NSLayoutConstraint constraintWithItem:nReviews attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.starPlaceholder attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0];
    [self.cv addConstraint:lc];
    lc = [NSLayoutConstraint constraintWithItem:nReviews attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.starPlaceholder attribute:NSLayoutAttributeBottom multiplier:1.0 constant:starsReviewLabelMargin];
    [self.cv addConstraint:lc];
    
    
    // create "View on..." button
    CGFloat reviewLabelViewButtonMargin = 14.0;
    UIButton *viewOn = [UIButton buttonWithType:UIButtonTypeSystem];
    viewOn.translatesAutoresizingMaskIntoConstraints = NO;
    [viewOn setTitle:@"View on" forState:UIControlStateNormal];
    viewOn.titleLabel.font = [UIFont systemFontOfSize:12.0];
    [viewOn sizeToFit];
    [viewOn addTarget:self action:@selector(launchYelp) forControlEvents:UIControlEventTouchUpInside];
    [self.cv addSubview:viewOn];
    // configure position with constraints
    lc = [NSLayoutConstraint constraintWithItem:viewOn attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:viewOn.frame.size.width];
    [self.cv addConstraint:lc];
    lc = [NSLayoutConstraint constraintWithItem:viewOn attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:viewOn.frame.size.height];
    [self.cv addConstraint:lc];
    lc = [NSLayoutConstraint constraintWithItem:viewOn attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:nReviews attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0];
    [self.cv addConstraint:lc];
    lc = [NSLayoutConstraint constraintWithItem:viewOn attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:nReviews attribute:NSLayoutAttributeBottom multiplier:1.0 constant:reviewLabelViewButtonMargin];
    [self.cv addConstraint:lc];
    

    // create logo image placeholder view
    CGFloat buttonLogoMargin = -4.0;
    
    self.logoButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.logoButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.logoButton setTitle:@"[Logo]" forState:UIControlStateNormal];
    self.logoButton.titleLabel.font = [UIFont systemFontOfSize:12.0];
    [self.logoButton sizeToFit];
    [self.logoButton addTarget:self action:@selector(launchYelp) forControlEvents:UIControlEventTouchUpInside];
    [self.cv addSubview:self.logoButton];
    // configure size and position with constraints
    lc = [NSLayoutConstraint constraintWithItem:self.logoButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:viewOn attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]; // have no idea why this does not work with NSLayoutAttributeRight
    [self.cv addConstraint:lc];
    lc = [NSLayoutConstraint constraintWithItem:self.logoButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:viewOn attribute:NSLayoutAttributeBottom multiplier:1.0 constant:buttonLogoMargin];
    [self.cv addConstraint:lc];
    lc = [NSLayoutConstraint constraintWithItem:self.logoButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:width];
    [self.cv addConstraint:lc];
    // load the actual image, if possible
    [SALUtilFuncs loadImageAsyncWithURL:kSALResourceTableViewCellYelpLogoURL andCallbackObj:self andSelector:@selector(loadLogoImage:)];
}


- (void) loadStarsImage:(UIImage *)img
{
    if (img == nil)
        return;
    
    CGSize imgSize = img.size;
    CGFloat aspectRatio = imgSize.width / imgSize.height;
    
    CGSize newSize;
    newSize.width  = self.starPlaceholder.frame.size.width;
    newSize.height = newSize.width / aspectRatio;
    CGRect newFrame = self.starPlaceholder.frame;
    newFrame.size = newSize;
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
    
    self.starPlaceholder.frame = newFrame;
    newFrame = imgView.frame;
    newFrame.size = newSize;
    imgView.frame = newFrame;
    
    [self.starPlaceholder addSubview:imgView];
    
    [self.cv setNeedsLayout];
    [self.cv layoutIfNeeded];
    [self updateContentViewHeight];
}


- (void) loadLogoImage:(UIImage *)img
{
    if (img == nil)
        return;
    
    CGSize imgSize = img.size;
    CGFloat aspectRatio = imgSize.width / imgSize.height;
    
    CGSize newSize;
    newSize.width  = self.logoButton.frame.size.width;
    newSize.height = newSize.width / aspectRatio;
    
    self.logoButton.titleLabel.font = [UIFont systemFontOfSize:3.0];
    [self.logoButton setTitle:@" " forState:UIControlStateNormal];
    [self.logoButton sizeToFit];
        
    UIImage *resizedImage = [SALUtilFuncs imageWithImage:img scaledToSize:newSize];
    [self.logoButton setBackgroundImage:resizedImage forState:UIControlStateNormal];
    [self.logoButton sizeToFit];
    
    [self.cv setNeedsLayout];
    [self.cv layoutIfNeeded];
    [self updateContentViewHeight];
}


#pragma mark - Convenience methods for upper left part of view

- (void) makeConfigureContactUpperLeftViewWithImage:(UIImage *)img
{
    CGFloat leftMargin = 10;
    CGFloat topMargin = 10;
    CGFloat rightMargin = -10;
    
    NSLayoutConstraint *lc;
    
    // create name label
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 20)];
    nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    nameLabel.text = self.resource.name;
    nameLabel.font = [UIFont systemFontOfSize:24.0];
    nameLabel.numberOfLines = 0;
    [self.cv addSubview:nameLabel];
    // configure position with constraints
    lc = [NSLayoutConstraint constraintWithItem:nameLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.cv attribute:NSLayoutAttributeLeft multiplier:1.0 constant:leftMargin];
    [self.cv addConstraint:lc];
    lc = [NSLayoutConstraint constraintWithItem:nameLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.cv attribute:NSLayoutAttributeTop multiplier:1.0 constant:topMargin];
    [self.cv addConstraint:lc];
    lc = [NSLayoutConstraint constraintWithItem:nameLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.starPlaceholder attribute:NSLayoutAttributeLeft multiplier:1.0 constant:rightMargin];
    [self.cv addConstraint:lc];
    [nameLabel sizeToFit];
    
    // create category label
    CGFloat nameCategoryMargin = 14.0;
    UILabel *categoryLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 20)];
    categoryLabel.translatesAutoresizingMaskIntoConstraints = NO;
    categoryLabel.text = self.resource.categoryStr;
    categoryLabel.font = [UIFont boldSystemFontOfSize:18.0];
    categoryLabel.numberOfLines = 0;
    [self.cv addSubview:categoryLabel];
    // configure position with constraints
    lc = [NSLayoutConstraint constraintWithItem:categoryLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.cv attribute:NSLayoutAttributeLeft multiplier:1.0 constant:leftMargin];
    [self.cv addConstraint:lc];
    lc = [NSLayoutConstraint constraintWithItem:categoryLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:nameLabel attribute:NSLayoutAttributeBottom multiplier:1.0 constant:nameCategoryMargin];
    [self.cv addConstraint:lc];
    lc = [NSLayoutConstraint constraintWithItem:categoryLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.starPlaceholder attribute:NSLayoutAttributeLeft multiplier:1.0 constant:rightMargin];
    [self.cv addConstraint:lc];
    [categoryLabel sizeToFit];
    
    // create phone button
    CGFloat categoryPhoneMargin = 8.0;
    UIButton *phoneButton = [UIButton buttonWithType:UIButtonTypeSystem];
    phoneButton.translatesAutoresizingMaskIntoConstraints = NO;
    [phoneButton setTitle:self.resource.displayPhone forState:UIControlStateNormal];
    phoneButton.titleLabel.font = [UIFont systemFontOfSize:14.0];
    [phoneButton sizeToFit];
    [phoneButton addTarget:self action:@selector(makeResourceCall) forControlEvents:UIControlEventTouchUpInside];
    [self.cv addSubview:phoneButton];
    // configure position with constraints
    lc = [NSLayoutConstraint constraintWithItem:phoneButton attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.cv attribute:NSLayoutAttributeLeft multiplier:1.0 constant:leftMargin];
    [self.cv addConstraint:lc];
    lc = [NSLayoutConstraint constraintWithItem:phoneButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:categoryLabel attribute:NSLayoutAttributeBottom multiplier:1.0 constant:categoryPhoneMargin];
    [self.cv addConstraint:lc];
    
    // create image (if available) and address field
    CGSize svSize = self.cv.frame.size;
    CGFloat imageAddressTopMargin = 10.0;
    CGFloat imageAddressMargin = 10.0;
    self.addressLabel = [[SALCopyableUILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 20)];
    self.addressLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.addressLabel.text = [self.resource.addressStrArray componentsJoinedByString:@"\n"];
    self.addressLabel.font = [UIFont systemFontOfSize:14.0];
    self.addressLabel.numberOfLines = 0;
    [self.cv addSubview:self.addressLabel];
    if ([self.resource hasValidImageURL])
    {
        CGFloat imageAddressWidth = (svSize.width - leftMargin + rightMargin - imageAddressMargin) / 2.0;
        if (img == nil)
            img = [UIImage imageNamed:kSALResourceTableViewCellDefaultImageName];
        UIImage *placeholderResized = [SALUtilFuncs imageWithImage:img scaledToWidth:imageAddressWidth];
        self.locationImage = [[UIImageView alloc] initWithImage:placeholderResized];
        self.locationImage.contentMode = UIViewContentModeScaleAspectFit;
        self.locationImage.translatesAutoresizingMaskIntoConstraints = NO;
        self.locationImage.frame = CGRectMake(0, 0, imageAddressWidth, imageAddressWidth);
        [self.cv addSubview:self.locationImage];
        lc = [NSLayoutConstraint constraintWithItem:self.locationImage attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.cv attribute:NSLayoutAttributeLeft multiplier:1.0 constant:leftMargin];
        [self.cv addConstraint:lc];
        lc = [NSLayoutConstraint constraintWithItem:self.locationImage attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:phoneButton attribute:NSLayoutAttributeBottom multiplier:1.0 constant:imageAddressTopMargin];
        [self.cv addConstraint:lc];
        lc = [NSLayoutConstraint constraintWithItem:self.locationImage attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.logoButton attribute:NSLayoutAttributeBottom multiplier:1.0 constant:imageAddressTopMargin];
        [self.cv addConstraint:lc];
        lc = [NSLayoutConstraint constraintWithItem:self.locationImage attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:imageAddressWidth];
        [self.cv addConstraint:lc];
        // load the actual image, if possible
        [self tryToLoadYelpLargeImage];
        
        lc = [NSLayoutConstraint constraintWithItem:self.addressLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.locationImage attribute:NSLayoutAttributeRight multiplier:1.0 constant:imageAddressMargin];
        [self.cv addConstraint:lc];
        lc = [NSLayoutConstraint constraintWithItem:self.addressLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:imageAddressWidth];
        [self.cv addConstraint:lc];
        lc = [NSLayoutConstraint constraintWithItem:self.addressLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.locationImage attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0];
        [self.cv addConstraint:lc];
    }
    else
    {
        self.locationImage = nil; //just to be sure
        lc = [NSLayoutConstraint constraintWithItem:self.addressLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.cv attribute:NSLayoutAttributeLeft multiplier:1.0 constant:leftMargin];
        [self.cv addConstraint:lc];
        lc = [NSLayoutConstraint constraintWithItem:self.addressLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.cv attribute:NSLayoutAttributeRight multiplier:1.0 constant:rightMargin];
        [self.cv addConstraint:lc];
        lc = [NSLayoutConstraint constraintWithItem:self.addressLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:phoneButton attribute:NSLayoutAttributeBottom multiplier:1.0 constant:imageAddressTopMargin];
        [self.cv addConstraint:lc];
        lc = [NSLayoutConstraint constraintWithItem:self.addressLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.logoButton attribute:NSLayoutAttributeBottom multiplier:1.0 constant:imageAddressTopMargin];
        [self.cv addConstraint:lc];
    }
}


- (void) tryToLoadYelpLargeImage
{
    NSString *origURL = self.resource.imageURL;
    NSURL *tmpURL = [[NSURL URLWithString:origURL] URLByDeletingLastPathComponent];
    NSString *newURL = [[tmpURL URLByAppendingPathComponent:@"l.jpg"] absoluteString];
    [SALUtilFuncs loadImageAsyncWithURL:newURL andCallbackObj:self andSelector:@selector(checkYelpImageOrFallbackToOriginal:)];
}


- (void) checkYelpImageOrFallbackToOriginal:(UIImage *)img
{
    if (img != nil)
        [self loadLocationImage:img];
    else
    {
        NSString *origURL = self.resource.imageURL;
        NSURL *tmpURL = [[NSURL URLWithString:origURL] URLByDeletingLastPathComponent];
        NSString *newURL = [[tmpURL URLByAppendingPathComponent:@"o.jpg"] absoluteString];
        [SALUtilFuncs loadImageAsyncWithURL:newURL andCallbackObj:self andSelector:@selector(checkYelpImageOrFallbackToRegular:)];
    }
}


- (void) checkYelpImageOrFallbackToRegular:(UIImage *)img
{
    if (img != nil)
        [self loadLocationImage:img];
    else
        [SALUtilFuncs loadImageAsyncWithURL:self.resource.imageURL andCallbackObj:self andSelector:@selector(loadLocationImage:)];
}


- (void) loadLocationImage:(UIImage *)img
{
    if (img==nil)
        return;
    
    CGRect frame = self.locationImage.frame;
    UIImage *resizedImage = [SALUtilFuncs imageWithImage:img scaledToWidth:frame.size.width];
    frame.size.height = resizedImage.size.height;
    self.locationImage.image = resizedImage;
    
    [self.cv setNeedsLayout];
    [self.cv layoutIfNeeded];
    [self updateContentViewHeight];
}


#pragma mark - Convenience methods for map view

- (void) makeConfigureMapView
{
    CGFloat leftMargin = 10;
    CGFloat topMargin = 10;
    CGFloat rightMargin = 10;
    CGSize  svSize = self.cv.frame.size;
    
    CGFloat mapHeightWidth = svSize.width - leftMargin - rightMargin;
    self.mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, mapHeightWidth, mapHeightWidth)];
    self.mapView.translatesAutoresizingMaskIntoConstraints = NO;
    self.mapView.delegate = self;
    self.mapView.showsUserLocation = YES;
    self.mapView.zoomEnabled = NO;
    self.mapView.scrollEnabled = NO;
    self.mapView.pitchEnabled = NO;
    self.mapView.rotateEnabled = NO;
    
    MKCoordinateRegion localRegion;
    localRegion = MKCoordinateRegionMakeWithDistance(self.resource.requestCoord,
                                                     self.resource.distanceMeters*2*kSALResourceSubViewDisplayRadiusMultiplier,
                                                     self.resource.distanceMeters*2*kSALResourceSubViewDisplayRadiusMultiplier);
    [self.mapView setRegion:localRegion animated:NO];
    [self.cv addSubview:self.mapView];
    [SALLocationFinder findMostLikelyCoordForAddressStrArray:self.resource.addressStrArray
                                                    andCoord:self.resource.requestCoord
                                                withDistance:self.resource.distanceMeters
                                                 callbackObj:self
                                                 andSelector:@selector(updateMapViewWithPlacemark:)];
    
    NSLayoutConstraint *lc;
    lc = [NSLayoutConstraint constraintWithItem:self.mapView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.addressLabel attribute:NSLayoutAttributeBottom multiplier:1.0 constant:topMargin];
    [self.cv addConstraint:lc];
    lc = [NSLayoutConstraint constraintWithItem:self.mapView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.cv attribute:NSLayoutAttributeLeft multiplier:1.0 constant:leftMargin];
    [self.cv addConstraint:lc];
    lc = [NSLayoutConstraint constraintWithItem:self.mapView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:mapHeightWidth];
    [self.cv addConstraint:lc];
    lc = [NSLayoutConstraint constraintWithItem:self.mapView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:mapHeightWidth];
    [self.cv addConstraint:lc];
    if (self.locationImage != nil)
    {
        lc = [NSLayoutConstraint constraintWithItem:self.mapView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.locationImage attribute:NSLayoutAttributeBottom multiplier:1.0 constant:topMargin];
        [self.cv addConstraint:lc];
    }
    
    CGFloat mapLinkMargin = 4.0;
    self.mapLink = [UIButton buttonWithType:UIButtonTypeSystem];
    self.mapLink.translatesAutoresizingMaskIntoConstraints = NO;
    [self.mapLink setTitle:@"View in iOS Maps" forState:UIControlStateNormal];
    self.mapLink.titleLabel.font = [UIFont systemFontOfSize:14.0];
    [self.mapLink sizeToFit];
    [self.mapLink addTarget:self action:@selector(launchMapsApp) forControlEvents:UIControlEventTouchUpInside];
    
    [self.cv addSubview:self.mapLink];
    lc = [NSLayoutConstraint constraintWithItem:self.mapLink attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:self.mapLink.frame.size.width];
    [self.cv addConstraint:lc];
    lc = [NSLayoutConstraint constraintWithItem:self.mapLink attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:self.mapLink.frame.size.height];
    [self.cv addConstraint:lc];
    lc = [NSLayoutConstraint constraintWithItem:self.mapLink attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.cv attribute:NSLayoutAttributeLeft multiplier:1.0 constant:leftMargin];
    [self.cv addConstraint:lc];
    lc = [NSLayoutConstraint constraintWithItem:self.mapLink attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.mapView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:mapLinkMargin];
    [self.cv addConstraint:lc];
    
    [self.cv layoutIfNeeded];
    [self updateContentViewHeight];
}


- (void) updateMapViewWithPlacemark:(CLPlacemark *)placemark
{
    if (placemark == nil)
        return;
    
    CLLocation *location = placemark.location;
    self.locationPlacemark = placemark;
    
    MKPointAnnotation *startPoint = [[MKPointAnnotation alloc] init];
    startPoint.coordinate = self.resource.requestCoord;
    [self.mapView addAnnotation:startPoint];
    
    MKPointAnnotation *endPoint = [[MKPointAnnotation alloc] init];
    endPoint.coordinate = location.coordinate;
    [self.mapView addAnnotation:endPoint];
    
    self.locationCoord = location.coordinate;
    
    
    // update map frame to encompass both points, plus a little
    CLLocationCoordinate2D startCoord = startPoint.coordinate;
    CLLocationCoordinate2D endCoord = endPoint.coordinate;
    MKUserLocation *userLoc = self.mapView.userLocation;
    
    CLLocationCoordinate2D coord1, coord2;
    
    if (userLoc == nil) // just do things based on the start and end coordinates if we can't get the user's location
    {
        coord1 = startCoord;
        coord2 = endCoord;
    }
    else
    {
        coord1 = [SALLocationFinder maxLatLongOfCoord:startCoord andCoord:endCoord];
        coord2 = [SALLocationFinder minLatLongOfCoord:startCoord andCoord:endCoord];
        
        CLLocationCoordinate2D userCoord = userLoc.coordinate;
        coord1 = [SALLocationFinder maxLatLongOfCoord:coord1 andCoord:userCoord];
        coord2 = [SALLocationFinder minLatLongOfCoord:coord2 andCoord:userCoord];
    }
    
    CLLocationCoordinate2D midpoint = [SALLocationFinder midpointBetweenCoord:coord1 andCoord:coord2];
    CLLocationDistance latDiffMeters = [SALLocationFinder latDistanceMetersBetweenCoord:coord1 andCoord:coord2];
    CLLocationDistance longDiffMeters = [SALLocationFinder longDistanceMetersBetweenCoord:coord1 andCoord:coord2];
    
    CLLocationDistance baseRadiusMeters;
    if (latDiffMeters > longDiffMeters)
        baseRadiusMeters = latDiffMeters;
    else
        baseRadiusMeters = longDiffMeters;
    
    if (baseRadiusMeters < kSALResourceSubViewMinimumBaseRadius)
        baseRadiusMeters = kSALResourceSubViewMinimumBaseRadius;
    
    MKCoordinateRegion newRegion = MKCoordinateRegionMakeWithDistance(midpoint,
                                                                      baseRadiusMeters * kSALResourceSubViewMapRangeScalefactor,
                                                                      baseRadiusMeters * kSALResourceSubViewMapRangeScalefactor);
    [self.mapView setRegion:newRegion animated:YES];
}


#pragma mark - MKMapView delegate methods

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKPointAnnotation *annot = annotation;
    CLLocationCoordinate2D annotCoord = annot.coordinate;
    if (annotCoord.latitude == self.locationCoord.latitude && annotCoord.longitude == self.locationCoord.longitude)
    {
        MKPinAnnotationView *startPin = [[MKPinAnnotationView alloc] initWithAnnotation:annot reuseIdentifier:nil];
        startPin.pinColor = MKPinAnnotationColorRed;
        startPin.animatesDrop = YES;
        return startPin;
    }
    
    if (annotCoord.latitude == self.resource.requestCoord.latitude && annotCoord.longitude == self.resource.requestCoord.longitude)
    {
        MKPinAnnotationView *startPin = [[MKPinAnnotationView alloc] initWithAnnotation:annot reuseIdentifier:nil];
        startPin.pinColor = MKPinAnnotationColorGreen;
        startPin.animatesDrop = YES;
        return startPin;
    }
    
    return  nil;
}


#pragma mark - Call resource function and UIAlertView delegate function

- (void) makeResourceCall
{
    NSString *messageStr;
    UIDevice *thisDevice = [UIDevice currentDevice];
    
    if (![thisDevice.model isEqualToString:@"iPhone"])
    {
        messageStr = @"We can only call out from mobile phone devices!";
        UIAlertView *notPhone=[[UIAlertView alloc] initWithTitle:@"No phone service"
                                                         message:messageStr
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
        [notPhone show];
        return;
        // in future: possibly auto-copy number to clipboard?
    }
    
    messageStr = [NSString stringWithFormat:@"Call %@ at [%@] now?", self.resource.name, self.resource.displayPhone];
    UIAlertView *confirmCall = [[UIAlertView alloc] initWithTitle:@"Confirm call"
                                                          message:messageStr
                                                         delegate:self
                                                cancelButtonTitle:@"Cancel"
                                                otherButtonTitles:@"CALL", nil];
    [confirmCall show];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
        return;
    
    NSString *callString = [NSString stringWithFormat:@"tel://%@",self.resource.phoneNumber];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:callString]];
}



/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
 */

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
