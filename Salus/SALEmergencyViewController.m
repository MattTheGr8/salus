//
//  SALEmergencyViewController.m
//  Salus
//
//  Created by Matthew Johnson on 4/23/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import "SALEmergencyViewController.h"
#import "SALEmergencyViewModel.h"

@implementation SALEmergencyViewController

- (id) init         //designated initializer
{
    self = [super init];
    if (self)
    {
        //initialize model ASAP to make sure it has time to load emergency info
        self.model = [[SALEmergencyViewModel alloc] initWithController:self];
        
        //set up main view
        CGRect appFrame = [[UIScreen mainScreen] applicationFrame];
        UIView *mainView = [[UIView alloc] initWithFrame:appFrame];
        mainView.backgroundColor = [UIColor whiteColor];
        self.view = mainView;
        
        //set up panic button image
        //self.imView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"panic_button.png"]];
        //[self.view addSubview:self.imView];
        //[self.imView setCenter:CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2)];
        UIImage *panicButtonImg = [UIImage imageNamed:kSALEmergencyViewPanicButtonImage];   // TK: dynamically unload from memory if necessary (and other images too?)
        self.imView = [UIButton buttonWithType:UIButtonTypeCustom];
        self.imView.userInteractionEnabled = YES;
        [self.imView setFrame:CGRectMake(0, 0, panicButtonImg.size.width, panicButtonImg.size.height)];
        [self.imView setImage:panicButtonImg forState:UIControlStateNormal];
        [self.imView addTarget:self action:@selector(emergencyButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.imView];
        [self.imView setCenter:CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2)];
        
        //set up upper text
        CGRect textFrame = CGRectMake(appFrame.origin.x + kSALEmergencyViewTextXMargin,
                                      appFrame.origin.y + kSALEmergencyViewUpperTextYMargin,
                                      appFrame.size.width - (2*kSALEmergencyViewTextXMargin),
                                      (appFrame.size.height - self.imView.frame.size.height)/2);
        self.upperText = [[UILabel alloc] initWithFrame:textFrame];
        self.upperText.text = kSALEmergencyViewUpperText;
        self.upperText.font = [UIFont boldSystemFontOfSize:kSALEmergencyViewTextSize];
        self.upperText.numberOfLines = 0;
        self.upperText.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:self.upperText];
        
        //set up lower text
        textFrame = CGRectMake(appFrame.origin.x + kSALEmergencyViewTextXMargin,
                               self.imView.frame.origin.y + self.imView.frame.size.height + kSALEmergencyViewLowerTextYMargin,
                               appFrame.size.width - (2*kSALEmergencyViewTextXMargin),
                               (appFrame.size.height - self.imView.frame.size.height)/2);
        self.lowerText = [[UILabel alloc] initWithFrame:textFrame];
        self.lowerText.text = [NSString stringWithFormat:kSALEmergencyViewLowerText, self.model.currentLocation];
        // at some point: if location comes back undefined, add a callback to change lower text if it comes in later
        self.lowerText.font = [UIFont boldSystemFontOfSize:kSALEmergencyViewTextSize];
        self.lowerText.numberOfLines = 0;
        self.lowerText.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:self.lowerText];
    }
    return self;
}

- (void) emergencyButtonPressed:(id)sender
{
    NSUInteger nNumbers = [self.model howManyEmergencyNumbers];
    if (nNumbers == 0)
    {
        UIAlertView *noNumbers=[[UIAlertView alloc] initWithTitle:@"No emergency numbers"
                                                          message:@"No emergency number data found for your current location!"
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [noNumbers show];
        return;
        // in future, hopefully the model object will be more robust (i.e. local database) and this will never happen
    }
    else if (nNumbers == 1)
    {
        self.numberToCall = [self.model onlyEmergencyNumber];
        self.typeOfNumber = [self.model onlyNumberType];
        [self makeEmergencyCall];
    }
    else
    {
        self.allNumberTypes = [[self.model allNumberTypes] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
        NSMutableArray *allNums = [NSMutableArray arrayWithCapacity:nNumbers];
        NSMutableArray *allTypesAndNumbers = [NSMutableArray arrayWithCapacity:nNumbers];
        NSString *tempNumber;
        NSString *tempTypeAndNumber;
        for (NSUInteger i = 0; i < nNumbers; i++)
        {
            tempNumber = [self.model emergencyNumberForNumberType:[self.allNumberTypes objectAtIndex:i]];
            if (tempNumber == nil)
            {
                UIAlertView *noNumbers=[[UIAlertView alloc] initWithTitle:@"Emergency number error"
                                                                  message:@"Error loading emergency numbers (sorry!)"
                                                                 delegate:nil
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles:nil];
                [noNumbers show];
                return;
            }
            [allNums addObject:tempNumber];
            tempTypeAndNumber = [NSString stringWithFormat:@"%@: %@",[self.allNumberTypes objectAtIndex:i],tempNumber];
            [allTypesAndNumbers addObject:tempTypeAndNumber];
        }
        self.allNumbers = [allNums copy];
        UIActionSheet *numberPicker = [[UIActionSheet alloc] initWithTitle:nil
                                                                  delegate:self
                                                         cancelButtonTitle:nil
                                                    destructiveButtonTitle:nil
                                                         otherButtonTitles:nil];
        for( NSString *tempButtonTitle in allTypesAndNumbers)
        {
            [numberPicker addButtonWithTitle:tempButtonTitle];
        }
        [numberPicker addButtonWithTitle:@"Cancel"];
        numberPicker.cancelButtonIndex = [allTypesAndNumbers count];
        [numberPicker showInView:[UIApplication sharedApplication].keyWindow];
    }
}

- (void) makeEmergencyCall
{
    NSString *messageStr;
    UIDevice *thisDevice = [UIDevice currentDevice];
    
    // /*
    if (![thisDevice.model isEqualToString:@"iPhone"])
    {
        messageStr = [NSString stringWithFormat:@"We can only call out from mobile phone devices. Please call %@ for [%@] emergency service on the closest available mobile phone!", self.numberToCall, self.typeOfNumber];
        UIAlertView *notPhone=[[UIAlertView alloc] initWithTitle:@"No phone service"
                                                         message:messageStr
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
        [notPhone show];
        return;
        // in future: possibly auto-copy emergency number to clipboard?
        // or actually, show this on the emergency screen in the first place if not a phone
    }
    // */
     
    messageStr = [NSString stringWithFormat:@"Are you SURE you want to call %@ for [%@] emergency service?", self.numberToCall, self.typeOfNumber];
    UIAlertView *confirmEmergencyCall = [[UIAlertView alloc] initWithTitle:@"Confirm emergency call"
                                                                   message:messageStr
                                                                  delegate:self
                                                         cancelButtonTitle:@"Cancel"
                                                         otherButtonTitles:@"CALL", nil];
    [confirmEmergencyCall show];
}

- (void) updateFromModel
{
    NSString *lowerTextTemp = [NSString stringWithFormat:kSALEmergencyViewLowerText, self.model.currentLocation];
    self.lowerText.text = lowerTextTemp;
}

#pragma mark - Delegate methods for alert view and action sheet

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
        return;
    
    NSString *callString = [NSString stringWithFormat:@"tel://%@",self.numberToCall];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:callString]];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex >= [self.allNumbers count])
        return;
    
    self.numberToCall = [self.allNumbers objectAtIndex:buttonIndex];
    self.typeOfNumber = [self.allNumberTypes objectAtIndex:buttonIndex];
    [self makeEmergencyCall];
}

@end
