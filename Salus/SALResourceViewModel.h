//
//  SALResourceViewModel.h
//  Salus
//
//  Created by Matthew Johnson on 9/22/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
@class SALResourceViewController;
@class SALLocationFinder;

#define kSALResourcesJSON_URL @"http://matthewrjohnson.net/salus/resources2023dummy.py"
#define kSALResourceUpdateDistanceMeters 100 // in meters


@interface SALResourceViewModel : NSObject

    @property (strong) NSMutableArray *resourceList; // will be an array of SALResourceViewModelItem
    @property (strong) SALResourceViewController *viewController;
    @property (assign) CLLocationCoordinate2D lastRequestCoord;
    @property (assign) CLLocationCoordinate2D coord;

    - (void) updateWithLocation:(CLLocation *)location;
    - (void) requestNewResourcesFromServer;
    - (void) parseJSONIntoResourcesArray:(NSData *)jsonData;
@end
