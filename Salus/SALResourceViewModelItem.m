//
//  SALResourceViewModelItem.m
//  Salus
//
//  Created by Matthew Johnson on 10/4/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import "SALResourceViewModelItem.h"
#import <MapKit/MapKit.h>

@implementation SALResourceViewModelItem



+ (SALResourceViewModelItem *) resourceItemFromDictionary:(NSDictionary *)dict
{
    // format coming in should be a dictionary with keys:
    // category: (string)
    // name: (string)
    // display_phone: (string)
    // phone: (string)
    // distance_m: (float)
    // image_url: (string)
    // address: (string)
    // yelp_link: (string)
    // rating: (string) -- that's for the image URL
    // review_count: (string? although should be int)
    
    id tempVal;
    
    // unpack category
    tempVal = [SALResourceViewModelItem getCheckItemFromDictionary:dict withKey:kSALResourceViewItemCategoryKey andClass:[NSString class]];
    mSALResourceViewReturnNilIfTempValNil
    NSString *categoryStr = tempVal;
    NSUInteger categoryInt = [SALResourceViewModelItem categoryCodeForServerCategoryString:categoryStr];
    
    // unpack resource name
    tempVal = [SALResourceViewModelItem getCheckItemFromDictionary:dict withKey:kSALResourceViewItemNameKey andClass:[NSString class]];
    mSALResourceViewReturnNilIfTempValNil
    NSString *nameStr = tempVal;
    
    // unpack display phone #
    tempVal = [SALResourceViewModelItem getCheckItemFromDictionary:dict withKey:kSALResourceViewItemDisplayPhoneKey andClass:[NSString class]];
    mSALResourceViewReturnNilIfTempValNil
    NSString *displayPhoneStr = tempVal;
    
    // unpack regular phone #
    tempVal = [SALResourceViewModelItem getCheckItemFromDictionary:dict withKey:kSALResourceViewItemPhoneKey andClass:[NSString class]];
    mSALResourceViewReturnNilIfTempValNil
    NSString *phoneStr = tempVal;
    
    // unpack distance
    tempVal = [SALResourceViewModelItem getCheckItemFromDictionary:dict withKey:kSALResourceViewItemDistanceKey andClass:[NSNumber class]];
    mSALResourceViewReturnNilIfTempValNil
    NSNumber *tempNumber = tempVal;
    float distanceMeters = [tempNumber floatValue];
    
    // unpack image URL
    tempVal = [SALResourceViewModelItem getCheckItemFromDictionary:dict withKey:kSALResourceViewItemImageURLKey andClass:[NSString class]];
    mSALResourceViewReturnNilIfTempValNil
    NSString *imageURLStr = tempVal;
    
    // unpack address
    tempVal = [SALResourceViewModelItem getCheckItemFromDictionary:dict withKey:kSALResourceViewItemAddressKey andClass:[NSArray class]];
    mSALResourceViewReturnNilIfTempValNil
    NSArray *addressStrArray = tempVal;
    
    // unpack Yelp link [assuming everything is Yelp for now]
    tempVal = [SALResourceViewModelItem getCheckItemFromDictionary:dict withKey:kSALResourceViewItemYelpLinkKey andClass:[NSString class]];
    mSALResourceViewReturnNilIfTempValNil
    NSString *yelpLinkStr = tempVal;
    
    // unpack rating (star) image URL [again, assuming Yelp]
    tempVal = [SALResourceViewModelItem getCheckItemFromDictionary:dict withKey:kSALResourceViewItemRatingImageURLKey andClass:[NSString class]];
    mSALResourceViewReturnNilIfTempValNil
    NSString *ratingURLStr = tempVal;
    
    // unpack review count (assuming integer? will need to check)
    tempVal = [SALResourceViewModelItem getCheckItemFromDictionary:dict withKey:kSALResourceViewItemReviewCountKey andClass:[NSNumber class]];
    mSALResourceViewReturnNilIfTempValNil
    tempNumber = tempVal;
    NSUInteger reviewCount = [tempNumber unsignedIntegerValue];
    
    // make new resource item object and return
    SALResourceViewModelItem *newResource =     [[SALResourceViewModelItem alloc] init];
    newResource.category =                      categoryInt;
    newResource.name =                          nameStr;
    newResource.displayPhone =                  displayPhoneStr;
    newResource.phoneNumber =                   phoneStr;
    newResource.distanceMeters =                distanceMeters;
    newResource.imageURL =                      imageURLStr;
    newResource.addressStrArray =               addressStrArray;
    newResource.yelpLink =                      yelpLinkStr;
    newResource.ratingImageURL =                ratingURLStr;
    newResource.reviewCount =                   reviewCount;
    return newResource;
}


+ (id) getCheckItemFromDictionary:(NSDictionary *)dict withKey:(NSString *)key andClass:(id)aClass;
{
    id tempVal = [dict objectForKey:key];
    
    if (tempVal == [NSNull null] && aClass == [NSString class]) // allow "NULL" for strings, and return empty string
    {
        //NSLog(@"Null value for string key %@ in JSON; substituting empty string", key); // probably do some better error checking later...
        return @"";
    }
    
        if (tempVal == nil || ![tempVal isKindOfClass:aClass])
    {
        NSLog(@"Unable to parse %@ from JSON!", key); // probably do some better error checking later...
        return nil;
    }
    
    return tempVal;
}


+ (NSUInteger) categoryCodeForServerCategoryString:(NSString *)catStr
{
    if ([catStr isEqualToString:kSALResourceViewItemCategoryPharmacyServerStr])
        return kSALResourceViewItemCategoryPharmacy;
    else if ([catStr isEqualToString:kSALResourceViewItemCategoryDentistServerStr])
        return kSALResourceViewItemCategoryDentist;
    else if ([catStr isEqualToString:kSALResourceViewItemCategoryDoctorServerStr])
        return kSALResourceViewItemCategoryDoctor;
    else if ([catStr isEqualToString:kSALResourceViewItemCategoryChiroServerStr])
        return kSALResourceViewItemCategoryChiro;
    else if ([catStr isEqualToString:kSALResourceViewItemCategoryCounselingServerStr])
        return kSALResourceViewItemCategoryCounseling;
    else if ([catStr isEqualToString:kSALResourceViewItemCategoryERServerStr])
        return kSALResourceViewItemCategoryER;
    else if ([catStr isEqualToString:kSALResourceViewItemCategoryHospitalServerStr])
        return kSALResourceViewItemCategoryHospital;
    else if ([catStr isEqualToString:kSALResourceViewItemCategoryMidwifeServerStr])
        return kSALResourceViewItemCategoryMidwife;
    else if ([catStr isEqualToString:kSALResourceViewItemCategoryOptomServerStr])
        return kSALResourceViewItemCategoryOptom;
    else if ([catStr isEqualToString:kSALResourceViewItemCategoryRehabServerStr])
        return kSALResourceViewItemCategoryRehab;
    else if ([catStr isEqualToString:kSALResourceViewItemCategoryUCareServerStr])
        return kSALResourceViewItemCategoryUCare;
    else
        return 0;
}


+ (NSString *) categoryNameForCategoryCode:(NSUInteger)catCode
{
    if (catCode == kSALResourceViewItemCategoryPharmacy)
        return kSALResourceViewItemCategoryPharmacyStr;
    else if (catCode == kSALResourceViewItemCategoryDentist)
        return kSALResourceViewItemCategoryDentistStr;
    else if (catCode == kSALResourceViewItemCategoryDoctor)
        return kSALResourceViewItemCategoryDoctorStr;
    else if (catCode == kSALResourceViewItemCategoryChiro)
        return kSALResourceViewItemCategoryChiroStr;
    else if (catCode == kSALResourceViewItemCategoryCounseling)
        return kSALResourceViewItemCategoryCounselingStr;
    else if (catCode == kSALResourceViewItemCategoryER)
        return kSALResourceViewItemCategoryERStr;
    else if (catCode == kSALResourceViewItemCategoryHospital)
        return kSALResourceViewItemCategoryHospitalStr;
    else if (catCode == kSALResourceViewItemCategoryMidwife)
        return kSALResourceViewItemCategoryMidwifeStr;
    else if (catCode == kSALResourceViewItemCategoryOptom)
        return kSALResourceViewItemCategoryOptomStr;
    else if (catCode == kSALResourceViewItemCategoryRehab)
        return kSALResourceViewItemCategoryRehabStr;
    else if (catCode == kSALResourceViewItemCategoryUCare)
        return kSALResourceViewItemCategoryUCareStr;
    else
        return @"";
}


- (NSString *) categoryStr
{
    return [SALResourceViewModelItem categoryNameForCategoryCode:self.category];
}


- (NSString *) distanceStrLocalized
{
    // could definitely be better (i.e. have preferences), but this'll do for now
    
    MKDistanceFormatter *formatter = [[MKDistanceFormatter alloc] init];
    
    return [formatter stringFromDistance:self.distanceMeters];
}


- (NSString *) reviewCountStr
{
    // will need to be localized, someday...
    if (self.reviewCount == 0)
        return @"No reviews";
    else if (self.reviewCount == 1)
        return @"1 review";
    else
        return [NSString stringWithFormat:@"%lu reviews",(unsigned long)self.reviewCount];
}


- (NSComparisonResult)compareDistanceToOtherItem:(SALResourceViewModelItem *)other
{
    if (self.distanceMeters > other.distanceMeters)
        return NSOrderedDescending;

    if (self.distanceMeters < other.distanceMeters)
        return NSOrderedAscending;
    
    return NSOrderedSame;
}


- (BOOL) hasValidImageURL // convenience method
{
    if (self.imageURL == nil)
        return NO;
    
    if ([self.imageURL isEqualToString:@""])
        return NO;
    
    return YES;
}


@end
