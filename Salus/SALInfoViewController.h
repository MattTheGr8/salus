//
//  SALInfoViewController.h
//  Salus
//
//  Created by Matthew Johnson on 4/22/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SALInfoViewController : UIViewController

@property (strong) UIImageView *imView;

@end
