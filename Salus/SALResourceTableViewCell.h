//
//  SALResourceTableViewCell.h
//  Salus
//
//  Created by Matthew Johnson on 10/4/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SALResourceViewModelItem;

#define kSALResourceTableViewCellDefaultImageName @"resource_thumb_default"
#define kSALResourceTableViewCellThumbnailXOffset 4
#define kSALResourceTableViewCellThumbnailYOffset 6
#define kSALResourceTableViewCellThumbnailSize 32

#define kSALResourceTableViewCellRatingXOffset 32
#define kSALResourceTableViewCellRatingYOffset 10
#define kSALResourceTableViewCellRatingWidth 30

#define kSALResourceTableViewCellYelpLogoURL @"http://s3-media3.fl.yelpcdn.com/assets/2/www/img/2d7ab232224f/developers/yelp_logo_100x50.png"
#define kSALResourceTableViewCellYelpLogoYOffset 20

@interface SALResourceTableViewCell : UITableViewCell

    @property (strong) UIImageView *thumbnail;
    @property (strong) UIImageView *rating;
    @property (strong) UIImageView *logo;
    @property (assign) CGRect ratingRect;
    @property (assign) CGRect logoRect;

    - (void) configureFromModelItem:(SALResourceViewModelItem *)item; //to come
    - (void) resetThumbImage;
    - (void) loadThumbnailAsync:(NSString *)thumbURL;
    - (void) doThumbnailWithImage:(UIImage *)img;
    - (void) loadRatingAsync:(NSString *)ratingURL;
    - (void) doRatingWithImage:(UIImage *)img;
    - (void) loadYelpLogoAsync;
    - (void) doYelpLogoWithImage:(UIImage *)img;
    - (CGRect) rectForThumbnail;
    - (CGRect) rectForRatingImage:(UIImage *)img;
    - (CGRect) rectForLogoImage:(UIImage *)img;
@end
