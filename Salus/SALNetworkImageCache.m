//
//  SALNetworkImageCache.m
//  Salus
//
//  Created by Matthew Johnson on 10/12/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import "SALNetworkImageCache.h"
#import "SALAppDelegate.h"

@implementation SALNetworkImageCache

- (id) init         //designated initializer
{
    self = [super init];
    if (self)
    {
        self.urlImgDict = [[NSMutableDictionary alloc] init];
    }
    return self;
}

+ (UIImage *) globalCacheGetImageForURL:(NSString *)url
{
    SALAppDelegate *sharedAppDelegate = [[UIApplication sharedApplication] delegate];
    SALNetworkImageCache *globalCache = sharedAppDelegate.globalImageCache;
    
    return [globalCache.urlImgDict objectForKey:url];
}


+ (void) globalCacheSetImage:(UIImage *)img forURL:(NSString *)url;
{
    SALAppDelegate *sharedAppDelegate = [[UIApplication sharedApplication] delegate];
    SALNetworkImageCache *globalCache = sharedAppDelegate.globalImageCache;
    
    [globalCache.urlImgDict setObject:img forKey:url];
}

+ (BOOL) globalCacheAlreadyHas:(NSString *)url
{
    return ([SALNetworkImageCache globalCacheGetImageForURL:url] != nil);
}



@end
