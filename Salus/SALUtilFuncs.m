//
//  SALUtilFuncs.m
//  Salus
//
//  Created by Matthew Johnson on 10/16/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import "SALUtilFuncs.h"
#import "SALNetworkImageCache.h"

@implementation SALUtilFuncs


+ (void) loadImageAsyncWithURL:(NSString *)urlStr andCallbackObj:(id)obj andSelector:(SEL)sel;
{
    if ([SALNetworkImageCache globalCacheAlreadyHas:urlStr])
    {
        [obj performSelectorOnMainThread:sel withObject:[SALNetworkImageCache globalCacheGetImageForURL:urlStr] waitUntilDone:NO];
        return;
    }
    
    // create URL request
    NSURL *url = [[NSURL alloc] initWithString:urlStr];
    NSURLRequest *urlReq = [[NSURLRequest alloc] initWithURL:url];
    
    // call a function to parse returned JSON into annotation objects and call that from our completion handler
    //  if that is successful, send the received annotations back to the view controller
    [NSURLConnection sendAsynchronousRequest:urlReq queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
             // OK, in this utility function, let's just return nil to the selector in question, and let it handle the error
             [obj performSelectorOnMainThread:sel withObject:nil waitUntilDone:NO];
         else {
             UIImage *img = [UIImage imageWithData:data];
             
             [obj performSelectorOnMainThread:sel withObject:img waitUntilDone:NO];
             [SALNetworkImageCache globalCacheSetImage:img forURL:urlStr];
         }
     }];
    
}


// mostly/entirely ripped off from: http://stackoverflow.com/questions/2658738/the-simplest-way-to-resize-an-uiimage
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToWidth:(CGFloat)newWidth
{
    CGSize inSize = image.size;
    CGFloat aspectRat = inSize.width / inSize.height;
    CGFloat newHeight = newWidth / aspectRat;
    return [SALUtilFuncs imageWithImage:image scaledToSize:CGSizeMake(newWidth, newHeight)];
}



#pragma mark - Yelp-related utilities

// taken straight from http://www.yelp.com/developers/documentation/v2/iphone
+ (BOOL) isYelpInstalled
{
    return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"yelp:"]];
}

+ (void) openYelpURL:(NSString *)urlStr
{
    if ([SALUtilFuncs isYelpInstalled])
    {
        NSRange range = [urlStr rangeOfString:@"biz"];
        
        if (range.location != NSNotFound)       //transmogrify URL into Yelp URL
            urlStr = [NSString stringWithFormat:@"yelp:///%@", [urlStr substringFromIndex:range.location]];
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
}



@end
