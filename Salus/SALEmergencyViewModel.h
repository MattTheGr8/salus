//
//  SALEmergencyViewModel.h
//  Salus
//
//  Created by Matthew Johnson on 5/8/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import <Foundation/Foundation.h>
@class SALEmergencyViewController;
@class SALLocationFinder;

//#define kSALEmergencyPhoneNumberPlaceholder @"860-978-6081"
//#define kSALEmergencyPhoneNumberJSON_URL @"http://localhost:8888/cgi-bin/emergency.py"
#define kSALEmergencyPhoneNumberJSON_URL @"http://matthewrjohnson.net/salus/emergency.py"
#define kSALEmergencyPhoneNumberLocationKey @"location"
#define kSALEmergencyPhoneNumberLocationUnknownString @"<unknown>"
#define kSALEmergencyPhoneNumberNumbersKey @"numbers"

@interface SALEmergencyViewModel : NSObject

    @property (strong) NSDictionary *emergencyNumberDict;
    @property (strong) NSString *currentLocation;
    @property (strong) SALEmergencyViewController *viewController;
    @property (strong) NSString *countryCode;

    - (id) initWithController:(SALEmergencyViewController *) controller;
    //+ (NSString *) emergencyPhoneNumber; //phase this out soon
    - (void) parseJSONIntoDict:(NSData *)jsonData;
    - (void) clearAllData;
    - (NSUInteger) howManyEmergencyNumbers;
    - (NSString *) onlyNumberType;
    - (NSString *) onlyEmergencyNumber;
    - (NSArray *) allNumberTypes;
    - (NSString *) emergencyNumberForNumberType:(NSString *)type;
    - (void) updateCountryCode:(NSString *)newCode;
@end
