//
//  SALAppDelegate.m
//  Salus
//
//  Created by Matthew Johnson on 4/17/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import "SALAppDelegate.h"
#import "SALLocationFinder.h"
#import "SALMapViewController.h"
#import "SALInfoViewController.h"
#import "SALResourceViewController.h"
#import "SALReportViewController.h"
#import "SALEmergencyViewController.h"
#import "SALNetworkImageCache.h"

@implementation SALAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    self.locFinder = [[SALLocationFinder alloc] init];
    self.globalImageCache = [[SALNetworkImageCache alloc] init];
    
    self.tabBarController = [[UITabBarController alloc] init];
    self.mapVC = [[SALMapViewController alloc] init];
    UITabBarItem *tbi1 = [[UITabBarItem alloc] initWithTitle:@"Map" image:[UIImage imageNamed:@"globe-30.png"] tag:0];
    self.mapVC.tabBarItem = tbi1;
    
    SALInfoViewController *vc2 = [[SALInfoViewController alloc] init];
    UITabBarItem *tbi2 = [[UITabBarItem alloc] initWithTitle:@"Info" image:[UIImage imageNamed:@"info-30.png"] tag:1];
    vc2.tabBarItem = tbi2;
    
    SALResourceViewController *vc3 = [[SALResourceViewController alloc] init];
    UITabBarItem *tbi3 = [[UITabBarItem alloc] initWithTitle:@"Resources" image:[UIImage imageNamed:@"contact_card-30.png"] tag:2];
    vc3.tabBarItem = tbi3;
    
    SALReportViewController *reportVC = [[SALReportViewController alloc] initWithStyle:UITableViewStylePlain];
    UINavigationController *vc4 = [[UINavigationController alloc] initWithRootViewController:reportVC];
    vc4.delegate = reportVC;
    UITabBarItem *tbi4 = [[UITabBarItem alloc] initWithTitle:@"Report" image:[UIImage imageNamed:@"treatment_plan-30.png"] tag:3];
    vc4.tabBarItem = tbi4;
    
    SALEmergencyViewController *vc5 = [[SALEmergencyViewController alloc] init];
    UITabBarItem *tbi5 = [[UITabBarItem alloc] initWithTitle:@"Emergency" image:[UIImage imageNamed:@"ambulance-30.png"] tag:4];
    vc5.tabBarItem = tbi5;
    
    NSArray *controllers = [NSArray arrayWithObjects:self.mapVC, vc2, vc3, vc4, vc5, nil];
    self.tabBarController.viewControllers = controllers;
    self.window.rootViewController = self.tabBarController;
//    
//    UITabBar *tempTabBar = self.tabBarController.tabBar;
//    CGFloat tabBarHeight = tempTabBar.frame.size.height;
//    NSString *logStr = [NSString stringWithFormat:@"Tab bar height: %f\n", tabBarHeight];
//    NSLog(logStr);
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    //NSLog(@"App entered background");
    [self.locFinder shutDown];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    if (self.inited)
    {
        //NSLog(@"Application became active");
        [self.locFinder checkAuthorizationAndInitStuff];
    }
    else
    {
        //NSLog(@"Application initializing");
        self.inited = YES;
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
