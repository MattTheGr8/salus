//
//  SALMapViewModel.h
//  Salus
//
//  Created by Matthew Johnson on 6/29/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
@class SALMapViewController;

//#define kSALMapAnnotationsJSON_URL @"http://localhost:8888/cgi-bin/map.py"
#define kSALMapAnnotationsJSON_URL @"http://matthewrjohnson.net/salus/map.py"

@interface SALMapViewModel : NSObject

    @property (assign) CLLocationCoordinate2D coord;
    @property (assign) CLLocationCoordinate2D lastAnnotationRequestCoord;
    @property (assign) MKCoordinateSpan mapSpan;
    @property (strong) SALMapViewController *viewController;
    @property (strong) NSMutableArray *annotations;     // for now, we'll just replace all the annotations when we update;
                                                        //   TK: grow dynamically (e.g. in case user scrolls back)
                                                        //   but deallocate as necessary if memory gets tight
                                                        // on a related note, think about doing requests from server more intelligently --
                                                        //   i.e., if the new span overlaps with
                                                        //   the old span, only request the non-overlapping portion?

    - (id) initWithController:(SALMapViewController *) controller;
    - (void) updateModelWithCoord:(CLLocationCoordinate2D) newCoord andSpan:(MKCoordinateSpan) newSpan force:(BOOL)doForce;
    - (BOOL) haveAnnotationsForNewSpan;
    - (void) requestNewAnnotationsFromServer;
    - (void) sendAnnotationsToViewController;
    - (void) parseJSONIntoAnnotationsArray:(NSData *)jsonData;

@end
