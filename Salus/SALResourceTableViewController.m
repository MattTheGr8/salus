//
//  SALResourceTableViewController.m
//  Salus
//
//  Created by Matthew Johnson on 10/2/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import "SALResourceTableViewController.h"
#import "SALResourceViewModel.h"
#import "SALResourceTableViewCell.h"
#import "SALResourceViewModelItem.h"
#import "SALResourceViewController.h"

@interface SALResourceTableViewController ()

@end

@implementation SALResourceTableViewController

- (id)initWithStyle:(UITableViewStyle)style andModel:(SALResourceViewModel *)model          //designated initializer
{
    self = [super initWithStyle:style];
    if (self) {
        self.model = model;
        self.title = kSALResourceTableViewTitle;
    }
    return self;
}

- (void) updateTableView
{
    if ([NSThread isMainThread] == NO)
    {
        [self performSelectorOnMainThread:@selector(updateTableView) withObject:nil waitUntilDone:NO];
        return;
    }
    
    [self.tableView reloadData];
}


#pragma mark - Table view data source and delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1; //this could change later but for now let's assume we are not grouping resources by type -- just one long list, sorted by time
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.model.resourceList count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil; //again, could change if we decide to group by type or something
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"SALResourceViewTableCell";
    SALResourceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil)
        cell = [[SALResourceTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle  reuseIdentifier:cellID];
    else
        [cell resetThumbImage];
    [cell configureFromModelItem:[self.model.resourceList objectAtIndex:indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SALResourceViewModelItem *modelItem = [self.model.resourceList objectAtIndex:indexPath.row];
    SALResourceViewController *parent = (id) self.parentViewController;
    SALResourceTableViewCell *theCell = (id) [tableView cellForRowAtIndexPath:indexPath];
    [parent createPushSubViewControllerForModelItem:modelItem withImage:theCell.thumbnail.image];
}




/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


/*
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
*/






@end
