//
//  SALReportViewController.h
//  Salus
//
//  Created by Matthew Johnson on 4/23/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SALReportViewModel;
@class SALReportViewModelScreen;

//#define kSALReportURL @"http://localhost:8888/cgi-bin/report.py"
#define kSALReportURL @"http://matthewrjohnson.net/salus/report.py"
#define kSALReportResultOKCode @"OK"
#define kSALReportResultBackgroundGray 0.8
#define kSALReportResultsTextXMargin 20
#define kSALReportResultsTextButtonBuffer 35
#define kSALReportResultsTextSize 20


@interface SALReportViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource, UINavigationControllerDelegate>

    //@property (strong) UIImageView *imView;
    @property (strong) SALReportViewModel *model;
    @property (strong) SALReportViewModelScreen *currentScreen;
    @property (weak) UIViewController *newlyCreatedViewController;
    @property (assign) BOOL safeToPushNewViewController;

    - (void) sendReportToServer:(NSString *)reportType;
    - (void) doReportResult:(NSString *)message;
    - (void) doTemporaryResultDisplay:(NSString *)message;
    - (void) displayResultMessage:(NSString *)message withReturnButton:(BOOL)buttonOn;
    - (void) parseServerResultCode:(NSData *)resultData;
    - (void) resetToRoot;
    - (void) safelyPushViewController:(UIViewController *)vc;

    // navigation controller delegate method (for handling back button)
    - (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated;
    - (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated;

    //data source & delegate protocol methods
    - (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
    - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
    - (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section;
    - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
    - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
@end
