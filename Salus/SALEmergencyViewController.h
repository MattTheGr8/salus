//
//  SALEmergencyViewController.h
//  Salus
//
//  Created by Matthew Johnson on 4/23/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SALEmergencyViewModel;

#define kSALEmergencyViewPanicButtonImage @"panic_button.png"
#define kSALEmergencyViewTextSize 20
#define kSALEmergencyViewTextXMargin 20
#define kSALEmergencyViewUpperTextYMargin 0
#define kSALEmergencyViewLowerTextYMargin -30
#define kSALEmergencyViewUpperText @"PRESS TO DIAL EMERGENCY SERVICES"
//#define kSALEmergencyViewLowerText @"NUMBER TO BE DIALED:\n%@"
#define kSALEmergencyViewLowerText @"USING NUMBERS FOR:\n%@"

@interface SALEmergencyViewController : UIViewController <UIAlertViewDelegate, UIActionSheetDelegate>

    //@property (strong) UIImageView *imView;
    @property (strong) UIButton *imView;
    @property (strong) UILabel *upperText;
    @property (strong) UILabel *lowerText;
    @property (strong) SALEmergencyViewModel *model;
    @property (strong) NSString *numberToCall;
    @property (strong) NSString *typeOfNumber;
    @property (strong) NSArray *allNumberTypes;
    @property (strong) NSArray *allNumbers;

    - (void) emergencyButtonPressed:(id)sender;
    - (void) makeEmergencyCall;
    - (void) updateFromModel;
    - (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex; //UIAlertViewDelegate method
    - (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex; //UIActionSheetDelegate method
@end
