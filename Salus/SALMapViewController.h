//
//  SALMapViewController.h
//  Salus
//
//  Created by Matthew Johnson on 4/17/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@class SALMapViewModel;
@class SALLocationFinder;

#define kSALMapViewZoomOutLimitMeters 241402 //150 miles in meters; this will be for either latitude or longitude (so corner-to-corner distance can be greater)

@interface SALMapViewController : UIViewController <MKMapViewDelegate>

    @property (strong) MKMapView *mapView;
    @property (assign) BOOL initialLocationSet;
    @property (strong) SALMapViewModel *model;
    @property (assign) BOOL pastInitialMapRender;
    @property (strong) SALLocationFinder *locFinder;

    - (void)updateModelAnnotationsForce:(BOOL)doForce; //slightly confusing name -- pass in "YES" to force an update, "NO" to have the controller use its judgment
    - (void)addModelAnnotationsToMapView;
    - (void)clearMapViewAnnotations;
    - (void)enableLocationDisplay;

    //MKMapView delegate methods
    - (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation;
    - (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation;
    - (void)mapViewDidFinishRenderingMap:(MKMapView *)mapView fullyRendered:(BOOL)fullyRendered;
    - (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated;
@end
