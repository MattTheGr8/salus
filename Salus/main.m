//
//  main.m
//  Salus
//
//  Created by Matthew Johnson on 4/17/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SALAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SALAppDelegate class]));
    }
}
