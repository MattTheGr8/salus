//
//  SALResourceTableViewController.h
//  Salus
//
//  Created by Matthew Johnson on 10/2/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SALResourceViewModel;

#define kSALResourceTableViewTitle  @"Resources"

@interface SALResourceTableViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>

    @property (strong) SALResourceViewModel *model;

    - (id)initWithStyle:(UITableViewStyle)style andModel:(SALResourceViewModel *)model;
    - (void) updateTableView;

    // Table view data source and delegate methods
    - (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
    - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
    - (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section;
    - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
    - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;

@end
