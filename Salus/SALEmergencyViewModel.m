//
//  SALEmergencyViewModel.m
//  Salus
//
//  Created by Matthew Johnson on 5/8/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import "SALEmergencyViewModel.h"
#import "SALEmergencyViewController.h"
#import "SALLocationFinder.h"
#import "SALAppDelegate.h"

@implementation SALEmergencyViewModel

- (id) initWithController:(SALEmergencyViewController *) controller         //designated initializer
{
    self = [super init];
    if (self)
    {
        [self clearAllData];
        self.viewController = controller;
        
        SALAppDelegate *sharedAppDelegate = [[UIApplication sharedApplication] delegate];
        
        SALLocationFinder *locFinder = sharedAppDelegate.locFinder;
        locFinder.emergencyModel = self;
        if (locFinder.haveInitialFix)
            [locFinder updateEmergencyModel];
        
        self.countryCode = @"";
    }
    return self;
}

//+ (NSString *) emergencyPhoneNumber
//{
//    return kSALEmergencyPhoneNumberPlaceholder;
//}

- (void) parseJSONIntoDict:(NSData *)jsonData
{
    //NSString* tempStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //NSLog(@"Here is the JSON data we got: %@\n", tempStr);
    
    NSError *parseError = nil;
    NSDictionary *tempDict = nil;
    tempDict = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&parseError];
    if (tempDict == nil || parseError != nil) //if there's an error, both should be true, but whatever
    {
        NSLog(@"JSON parsing error!"); // later: add in better error handling as above in init()
        [self clearAllData];
        return;
    }
    
    //get location
    id tempLocation = nil;
    tempLocation = [tempDict objectForKey:kSALEmergencyPhoneNumberLocationKey];
    if (tempLocation == nil || ![tempLocation isKindOfClass:[NSString class]])
    {
        NSLog(@"JSON parsing error!"); // later: add in better error handling as above in init()
        [self clearAllData];
        return;
    }
    self.currentLocation = tempLocation;
    
    //get numbers dictionary
    id tempDict2 = nil;
    tempDict2 = [tempDict objectForKey:kSALEmergencyPhoneNumberNumbersKey];
    if (tempDict2 == nil || ![tempDict2 isKindOfClass:[NSDictionary class]])
    {
        NSLog(@"JSON parsing error!"); // later: add in better error handling as above in init()
        [self clearAllData];
        return;
    }
    
    // later: make parsing even more sophisticated?
    NSUInteger nNumbers = [tempDict2 count];
    if (nNumbers < 1)
    {
        NSLog(@"No emergency numbers found in JSON dictionary!"); // later: add in better error handling as above in init()
        [self clearAllData];
        return;
    }
    // do a little basic parsing to make sure the dictionary is OK, and uppercase all number types for nice reading
    NSMutableDictionary *tempMut = [[NSMutableDictionary alloc] initWithCapacity:nNumbers];
    NSString *tempKey;
    id tempValue;
    NSArray *tempAllKeys = [tempDict2 allKeys];
    for (NSUInteger i = 0; i < nNumbers; i++)
    {
        tempKey = [tempAllKeys objectAtIndex:i];
        tempValue = [tempDict2 objectForKey:tempKey];
        if (tempValue == nil || ![tempValue isKindOfClass:[NSString class]])
        {
            NSLog(@"JSON parsing error!"); // later: add in better error handling as above in init()
            [self clearAllData];
            return;
        }
        [tempMut setObject:tempValue forKey:[tempKey capitalizedString]];
    }
    self.emergencyNumberDict = [NSDictionary dictionaryWithDictionary:tempMut];
    //[self.viewController updateFromModel];
    [self.viewController performSelectorOnMainThread:@selector(updateFromModel) withObject:nil waitUntilDone:NO];
     //need to do it this way rather than just send the message directly as we are calling from an asynchronous request
     // and thus we're not on the main thread. UI wasn't updating when we tried to send the message directly
}

- (void) clearAllData
{
    self.emergencyNumberDict = nil;
    self.currentLocation = kSALEmergencyPhoneNumberLocationUnknownString;
}

- (NSUInteger) howManyEmergencyNumbers
{
    if (self.emergencyNumberDict == nil)
        return 0;
    return [self.emergencyNumberDict count];
}

- (NSString *) onlyNumberType
// will only return an NSString if there is a single phone number in the dictionary; otherwise, returns nil
{
    if ([self howManyEmergencyNumbers] != 1)
        return nil;
    
    NSArray *keyArray = [self.emergencyNumberDict allKeys];
    NSString *keyToReturn = [keyArray objectAtIndex:0];
    return keyToReturn;
}

- (NSString *) onlyEmergencyNumber
// similar to above, only returns an NSString if there is a single phone number in the dictionary; otherwise, returns nil
{
    if ([self howManyEmergencyNumbers] != 1)
        return nil;
    
    NSString *onlyKey = [self onlyNumberType];
    NSString *onlyNumber = [self.emergencyNumberDict objectForKey:onlyKey];
    return onlyNumber;
}

- (NSArray *) allNumberTypes
{
    if ([self howManyEmergencyNumbers] < 1)
        return nil;
    
    return [self.emergencyNumberDict allKeys];
}

- (NSString *) emergencyNumberForNumberType:(NSString *)type
{
    if ([self howManyEmergencyNumbers] < 1)
        return nil;
    
    id value = [self.emergencyNumberDict valueForKey:type];
    if (value != nil && [value isKindOfClass:[NSString class]])
        return value;
    else
        return nil;
}

- (void) updateCountryCode:(NSString *)newCode
{
    self.countryCode = newCode;
    
    // now request new emergency number info from server
    NSURL *url = [[NSURL alloc] initWithString:kSALEmergencyPhoneNumberJSON_URL];
    NSMutableURLRequest *urlReq = [[NSMutableURLRequest alloc] initWithURL:url];
    NSString *postString = [NSString stringWithFormat:@"country_code=%@", self.countryCode];
    
    [urlReq setHTTPMethod:@"POST"];
    [urlReq setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    [NSURLConnection sendAsynchronousRequest:urlReq queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
             // at some future point:
             // 1. add fallback local database that gets updated when the app does in the App Store
             // 2. add a callback hook to keep trying
             NSLog(@"Failed to load emergency numbers URL");
         else {
             [self parseJSONIntoDict:data];
         }
     }];
}

@end
