//
//  SALMapViewAnnotation.h
//  Salus
//
//  Created by Matthew Johnson on 7/11/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

#define kSALMapViewAnnotationTypeFire 1
#define kSALMapViewAnnotationTypeEarthquake 2
#define kSALMapViewAnnotationTypeStormCell 3
#define kSALMapViewAnnotationTypeStormReport 4
#define kSALMapViewAnnotationTypeZombies 99 // temporary zombie icon taken from: http://icons.iconarchive.com/icons/martin-berube/character/128/Zombie-icon.png

#define kSALMapViewAnnotationTypeFireStr @"Fire"
#define kSALMapViewAnnotationTypeEarthquakeStr @"Earthquake"
#define kSALMapViewAnnotationTypeStormCellStr @"Storm Cell"
#define kSALMapViewAnnotationTypeStormReportStr @"Storm Report"
#define kSALMapViewAnnotationTypeZombieStr @"Zombie"

#define kSALMapViewAnnotationTypeFireServerStr @"fire"
#define kSALMapViewAnnotationTypeEarthquakeServerStr @"earthquake"
#define kSALMapViewAnnotationTypeStormCellServerStr @"stormcell"
#define kSALMapViewAnnotationTypeStormReportServerStr @"stormreport"
#define kSALMapViewAnnotationTypeZombieServerStr @"zombieism"

#define kSALMapViewAnnotationEventTypeKey @"event_type"
#define kSALMapViewAnnotationLatitudeKey @"latitude"
#define kSALMapViewAnnotationLongitudeKey @"longitude"
#define kSALMapViewAnnotationTimestampKey @"timestamp"
#define kSALMapViewAnnotationOtherKey @"other"

@interface SALMapViewAnnotation : NSObject <MKAnnotation>

    @property (assign,nonatomic) CLLocationCoordinate2D coordinate;
    @property (assign) BOOL isPoint; // as opposed to region
    //@property (assign) CGFloat distanceFromRequestOrigin; // I guess let's put it in meters to match CLLocation functions?
        //actually, let's not make this a property -- let's calculate it as needed instead from the centerCoord and a provided 2nd point
    @property (assign) NSUInteger eventType;
    @property (assign) NSTimeInterval timestamp; // in seconds since epoch time (1 Jan 1970 GMT); can easily be converted to/from NSDate
    @property (strong) NSDictionary *other;

    + (SALMapViewAnnotation *) mapViewAnnotationFromDictionary:(NSDictionary *)dict;
    + (NSString *) eventNameForEventType:(NSUInteger)type;
    + (NSString *) eventServerStrForEventType:(NSUInteger)type;
    + (NSUInteger) eventTypeCodeForServerEventTypeString:(NSString *)typeStr;
    - (CLLocationDistance) distanceFromRequestOrigin:(CLLocationCoordinate2D)ogin;
    - (NSString *) eventTypeStr;
    - (NSString *) eventTypeServerStr;
@end
