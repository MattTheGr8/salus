//
//  SALResourceViewModel.m
//  Salus
//
//  Created by Matthew Johnson on 9/22/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import "SALResourceViewModel.h"
#import "SALResourceViewController.h"
#import "SALAppDelegate.h"
#import "SALLocationFinder.h"
#import "SALMapViewModel.h"
#import "SALResourceViewModelItem.h"

@implementation SALResourceViewModel


- (id) init         //designated initializer
{
    self = [super init];
    if (self)
    {
        self.viewController = nil;
        self.resourceList = nil;
        
        NSLog(@"Initializing Resource model"); //debug
        
        SALAppDelegate *sharedAppDelegate = [[UIApplication sharedApplication] delegate];
        SALLocationFinder *locFinder = sharedAppDelegate.locFinder;
        locFinder.resourceModel = self;
        if (locFinder.haveInitialFix)
            [locFinder updateResourceModel];
        
    }
    return self;
}


- (void) updateWithLocation:(CLLocation *)location
{
    self.coord = location.coordinate;
    if (self.resourceList == nil)
    {
        [self requestNewResourcesFromServer];
        return;
    }
    
    CLLocationDistance dist = [SALLocationFinder distanceBetweenCLLocCoord:self.lastRequestCoord andCLLocCoord:self.coord];
    if (dist > kSALResourceUpdateDistanceMeters)
    {
        [self requestNewResourcesFromServer];
        return;
    }
    
    // for now, because Yelp doesn't give us lat/long, will comment this out and just re-request when we want to update display
    // later, we might try to locate the businesses by address and update even without requesting Yelp updates
    //[self.viewController updateFromModel];
}


- (void) requestNewResourcesFromServer
{
    // create URL request string based on current latitude/longitude
    NSURL *url = [[NSURL alloc] initWithString:kSALResourcesJSON_URL];
    NSMutableURLRequest *urlReq = [[NSMutableURLRequest alloc] initWithURL:url];
    NSString *postString = [NSString stringWithFormat:@"latitude=%f&longitude=%f", self.coord.latitude, self.coord.longitude];
    
    [urlReq setHTTPMethod:@"POST"];
    [urlReq setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    // call a function to parse returned JSON into annotation objects and call that from our completion handler
    //  if that is successful, send the received annotations back to the view controller
    [NSURLConnection sendAsynchronousRequest:urlReq queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
             // at some future point:
             // 1. add fallback local database that gets updated when the app does in the App Store
             // 2. add a callback hook to keep trying
             NSLog(@"Failed to load resources URL");
         else {
             [self parseJSONIntoResourcesArray:data];
             //[self.viewController updateWithCoordinate:self.coord]; // might have to do this on main thread? we'll see, I guess
             [self.viewController performSelectorOnMainThread:@selector(updateFromModel) withObject:nil waitUntilDone:NO];
         }
     }];
    
    // regardless of success, end with
    // (although for now we are depending on Yelp to give distances without an explicit latitude/longitude... so might not make a difference
    // let's comment it out for now and only call it if we're success
    // [self.viewController updateWithCoordinate:self.coord];
}


- (void) parseJSONIntoResourcesArray:(NSData *)jsonData
{
    NSError *parseError = nil;
    NSArray *allDataArray = nil;
    id arrayTemp = nil;
    arrayTemp = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&parseError];
    //NSLog(@"Temporary JSON received display: %@", arrayTemp);
    if (arrayTemp == nil || parseError != nil) //if there's an error, both should be true, but whatever
    {
        NSLog(@"JSON parsing error!"); // later: add in better error handling?
        // do anything else here to clean up? or retry request?
        return;
    }
    
    if ( ![arrayTemp isKindOfClass:[NSArray class] ] )
    {
        NSLog(@"Resources API did not return an array, as expected...");
        // additional cleanup or retry? (as above)
        return;
    }
    
    //if we've got this far, let's assign the data to an NSArray pointer so we can easily treat it as such
    allDataArray = arrayTemp;
    id resourceDictTemp;
    SALResourceViewModelItem *newResource;
    NSMutableArray *newResources = [[NSMutableArray alloc] init];
    for ( NSUInteger i = 0; i < [allDataArray count]; i++ )
    {
        resourceDictTemp = [allDataArray objectAtIndex:i];
        if (![resourceDictTemp isKindOfClass:[NSDictionary class]])
        {
            NSLog(@"An object within the array returned by the Resources API was not a dictionary, as expected...");
            // additional cleanup or retry? (as above)
            continue;
        }
        newResource = [SALResourceViewModelItem resourceItemFromDictionary:resourceDictTemp];
        if (newResource == nil)
            NSLog(@"Dictionary could not be converted to new resource item object..."); // again, handle more/better later
        else
        {
            newResource.requestCoord = self.coord;
            [newResources addObject:newResource];
        }
    }
    
    // if everything was successful, update resources and lastRequestCoord
    //[self.viewController clearMapViewAnnotations];
    
    [newResources sortUsingSelector:@selector(compareDistanceToOtherItem:)];
    self.resourceList = newResources;
    self.lastRequestCoord = self.coord;
}



@end
