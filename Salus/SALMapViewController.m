//
//  SALMapViewController.m
//  Salus
//
//  Created by Matthew Johnson on 4/17/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import "SALMapViewController.h"
#import "SALMapViewModel.h"
#import "SALMapViewAnnotation.h"
#import "SALAppDelegate.h"
#import "SALLocationFinder.h"

@implementation SALMapViewController

- (id) init         //designated initializer
{
    self = [super init];
    if (self)
    {
        self.initialLocationSet = NO;
        self.pastInitialMapRender = NO;
        CGRect appFrame = [[UIScreen mainScreen] applicationFrame];
        UIView *mainView = [[UIView alloc] initWithFrame:CGRectZero];
        mainView.backgroundColor = [UIColor whiteColor];
        self.view = mainView;
        self.mapView = [[MKMapView alloc] initWithFrame:appFrame];
        self.mapView.delegate = self;
        
        SALAppDelegate *sharedAppDelegate = [[UIApplication sharedApplication] delegate];
        self.locFinder = sharedAppDelegate.locFinder;
        if ([self.locFinder haveInitialFix])
            self.mapView.showsUserLocation = YES;
        else
            self.mapView.showsUserLocation = NO;
        self.locFinder.mapVC = self;
        
        [self.view addSubview:self.mapView];
        self.model = [[SALMapViewModel alloc] initWithController:self];
    }
    return self;
}

- (void)updateModelAnnotationsForce:(BOOL)doForce
{
    //CLLocationCoordinate2D coord = self.mapView.userLocation.coordinate;
    CLLocationCoordinate2D coord = self.mapView.region.center; // actually, probably want to do it this way -- they may scroll to a region that is not their location
    MKCoordinateSpan span = self.mapView.region.span;
    [self.model updateModelWithCoord:coord andSpan:span force:doForce];
}

- (void)addModelAnnotationsToMapView
{
//    BOOL onMainThread = [NSThread isMainThread];
//    if (onMainThread)
//        NSLog(@"Running addModelAnnotationsToMapView -- on main thread");
//    else
//        NSLog(@"Running addModelAnnotationsToMapView -- NOT ON MAIN THREAD!");
    
    if ([NSThread isMainThread] == NO)
    {
        [self performSelectorOnMainThread:@selector(addModelAnnotationsToMapView) withObject:nil waitUntilDone:NO];
        return;
    }
    
    NSArray *annotArray = self.model.annotations;
    
    for (NSUInteger i = 0; i < annotArray.count; i++)
    {
        [self.mapView addAnnotation:[annotArray objectAtIndex:i]];
    }
}

- (void)clearMapViewAnnotations
{
    if ([NSThread isMainThread] == NO)
    {
        [self performSelectorOnMainThread:@selector(clearMapViewAnnotations) withObject:nil waitUntilDone:NO];
        return;
    }
    
    //[self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView removeAnnotations:self.model.annotations];
}

- (void)enableLocationDisplay
{
    if (self.mapView != nil && self.mapView.showsUserLocation == NO)
        self.mapView.showsUserLocation = YES;
}

#pragma mark - Map view delegate functions

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    if (self.initialLocationSet)
        return; // eventually we'll probably want to change this -- need to appropriately update model/annotations when the user moves or resizes the view
    // actually, maybe not since we are now implementing mapViewDidFinishRenderingMap -- will depend on whether that is also called appropriately
    //  when the user location changes
    
    MKCoordinateRegion localRegion;
    localRegion.center = self.mapView.userLocation.coordinate;
    localRegion.span.latitudeDelta = 0.05;
    [self.mapView setRegion:localRegion animated:YES];
    self.initialLocationSet = YES;
    //[self updateModelAnnotations]; // should be able to get by without this; mapViewDidFinishRenderingMap should get called when animation finishes
}

- (void)mapViewDidFinishRenderingMap:(MKMapView *)mapView fullyRendered:(BOOL)fullyRendered
{
    if (!self.initialLocationSet)
    {
        if (!self.pastInitialMapRender)
        {
            self.pastInitialMapRender = YES;
            return;
        }
    }
    
    [self updateModelAnnotationsForce:NO];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    // A big chunk of this copied from Apple's "Location and Maps Programming Guide"
    
    // If the annotation is the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    // Handle any custom annotations.
    if ([annotation isKindOfClass:[SALMapViewAnnotation class]])
    {
        // Try to dequeue an existing view first.
        SALMapViewAnnotation *annot = annotation;
        NSString *annotID = [annot eventTypeServerStr];
        MKAnnotationView *annotView = [mapView dequeueReusableAnnotationViewWithIdentifier:annotID];
        
        if (!annotView)
        {
            // If an existing pin view was not available, create one.
            annotView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotID];
            NSString *imageName = [NSString stringWithFormat:@"%@%@", annotID, @".png"];
            annotView.image = [UIImage imageNamed:imageName];
            
            // If appropriate, customize the callout by adding accessory views (code not shown).
        }
        else
            annotView.annotation = annotation;
        
        return annotView;
    }
    
    return nil;
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    if (!self.initialLocationSet)
        return;
    
    // implement this to impose a limit on how far out user can zoom
    MKCoordinateSpan newSpan = mapView.region.span;
    CLLocationCoordinate2D newCenter = mapView.region.center;
    
    
    CLLocationCoordinate2D southEdge;
    southEdge.longitude = newCenter.longitude;
    southEdge.latitude = newCenter.latitude - newSpan.latitudeDelta;
    
    CLLocationCoordinate2D northEdge;
    northEdge.longitude = newCenter.longitude;
    northEdge.latitude = newCenter.latitude + newSpan.latitudeDelta;
    
    CLLocationDistance spanDist;
    spanDist = [SALLocationFinder distanceBetweenCLLocCoord:southEdge andCLLocCoord:northEdge];
    
    if (spanDist > kSALMapViewZoomOutLimitMeters)
    {
        MKCoordinateRegion newRegion = MKCoordinateRegionMakeWithDistance(newCenter, kSALMapViewZoomOutLimitMeters, 0);
        [mapView setRegion:newRegion animated:YES];
        return;
    }
    
    CLLocationCoordinate2D westEdge;
    westEdge.latitude = newCenter.latitude;
    westEdge.longitude = newCenter.longitude - newSpan.longitudeDelta;
    
    CLLocationCoordinate2D eastEdge;
    eastEdge.latitude = newCenter.latitude;
    eastEdge.longitude = newCenter.longitude + newSpan.longitudeDelta;
    
    spanDist = [SALLocationFinder distanceBetweenCLLocCoord:westEdge andCLLocCoord:eastEdge];
    
    if (spanDist > kSALMapViewZoomOutLimitMeters)
    {
        MKCoordinateRegion newRegion = MKCoordinateRegionMakeWithDistance(newCenter, 0, kSALMapViewZoomOutLimitMeters);
        [mapView setRegion:newRegion animated:NO];
    }
}


@end
