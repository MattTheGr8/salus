//
//  SALReportViewModelScreen.h
//  Salus
//
//  Created by Matthew Johnson on 8/28/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SALReportViewModelScreen : NSObject

    @property (strong) NSString *title;
    @property (weak) SALReportViewModelScreen *parent;
    @property (strong) NSArray *children; // array of SALReportViewModelScreen objects
    @property (strong) NSArray *childrenTitles; // array of strings

@end
