//
//  SALResourceSubViewController.h
//  Salus
//
//  Created by Matthew Johnson on 10/2/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@class SALResourceViewModelItem;
@class SALCopyableUILabel;

#define kSALResourceSubViewDisplayRadiusMultiplier 2 // if we think the address is 1km away, this will initially display 2km in each direction
#define kSALResourceSubViewBottomMargin 10.0 //in pixels
#define kSALResourceSubViewMapRangeScalefactor 1.5
#define kSALResourceSubViewMinimumBaseRadius 40 // in meters

@interface SALResourceSubViewController : UIViewController <MKMapViewDelegate, UIAlertViewDelegate>

    @property (strong) UIScrollView *sv;                    //short for scroll view
    @property (strong) UIView *cv;                          //short for content view
    @property (strong) SALResourceViewModelItem *resource;
    @property (strong) UIView *starPlaceholder;
    @property (strong) UIButton *logoButton;
    @property (strong) UIImageView *locationImage;
    //@property (strong) UILabel *addressLabel;
    @property (strong) SALCopyableUILabel *addressLabel;
    @property (strong) MKMapView *mapView;
    @property (strong) UIButton *mapLink;
    @property (assign) CLLocationCoordinate2D locationCoord;
    @property (strong) CLPlacemark *locationPlacemark;

    - (id) initWithResource:(SALResourceViewModelItem *)resource andImage:(UIImage *)img;       // designated initializer
    - (void) updateWithCoordinate:(CLLocationCoordinate2D)coord;
    - (void) updateContentViewHeight;
    - (void) launchMapsApp;
    - (void) launchYelp;

    // convenience methods for making various subviews
    - (void) makeConfigureYelpUpperRightView;
    - (void) loadStarsImage:(UIImage *)img;
    - (void) loadLogoImage:(UIImage *)img;

    - (void) makeConfigureContactUpperLeftViewWithImage:(UIImage *)img;
    - (void) tryToLoadYelpLargeImage;
    - (void) checkYelpImageOrFallbackToOriginal:(UIImage *)img;
    - (void) checkYelpImageOrFallbackToRegular:(UIImage *)img;
    - (void) loadLocationImage:(UIImage *)img;

    - (void) makeConfigureMapView;
    - (void) updateMapViewWithPlacemark:(CLPlacemark *)placemark;

    //MKMapView delegate methods
    - (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation;

    // for calling the resource in question
    - (void) makeResourceCall;
    - (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex; //UIAlertViewDelegate function
@end
