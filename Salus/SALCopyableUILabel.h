//
//  SALCopyableUILabel.h
//  Salus
//
//  Created by Matthew Johnson on 10/25/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SALCopyableUILabel : UILabel

@end
