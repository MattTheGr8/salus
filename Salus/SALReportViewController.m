//
//  SALReportViewController.m
//  Salus
//
//  Created by Matthew Johnson on 4/23/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import "SALReportViewController.h"
#import "SALReportViewModel.h"
#import "SALReportViewModelScreen.h"
#import "SALAppDelegate.h"
#import "SALLocationFinder.h"
#import "SALMapViewController.h"

@implementation SALReportViewController

- (id) initWithStyle:(UITableViewStyle)style         //designated initializer
{
    self = [super initWithStyle:style];
    if (self)
    {
        self.model = [[SALReportViewModel alloc] init];
        self.currentScreen = self.model.rootReportScreen;
        self.title = self.currentScreen.title;
        
        //self.navigationController.delegate = self;
        
        //UITableView *tableView = [[UITableView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame] style:UITableViewStylePlain];
        //tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        //tableView.delegate = self;
        //tableView.dataSource = self;
        //[tableView reloadData];
        //self.view = tableView;
        
//        CGRect appFrame = [[UIScreen mainScreen] applicationFrame];
//        UIView *mainView = [[UIView alloc] initWithFrame:CGRectZero];
//        mainView.backgroundColor = [UIColor whiteColor];
//        self.view = mainView;
//        self.imView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"report_tab.png"]];
//        CGRect imFrame = self.imView.frame;
//        imFrame.origin.x = appFrame.origin.x;
//        imFrame.origin.y = appFrame.origin.y;
//        self.imView.frame = imFrame;
//        [self.view addSubview:self.imView];
    }
    return self;
}

- (void) sendReportToServer:(NSString *)reportType
{
    // placeholder for now:
    // NSLog(@"Simulating sending report to server: %@", reportType);
    
    // first, make sure we can get location to report from
    SALAppDelegate *sharedAppDelegate = [[UIApplication sharedApplication] delegate];
    SALLocationFinder *locFinder = sharedAppDelegate.locFinder;
    if (!locFinder.haveInitialFix)
    {
        [self doReportResult:@"Sorry, an error occurred! (Could not get current location.) Please try again in a moment.\n\nIf you receive this message again, check to make sure location services are enabled for this app, and that your device is not in Airplane Mode."];
        return;
    }
    
    [self doTemporaryResultDisplay:@"Contacting server to report this incidence..."];
    CLLocationCoordinate2D coord = locFinder.locationManager.location.coordinate;
    
    // create URL request string based on current latitude/longitude
    NSURL *url = [[NSURL alloc] initWithString:kSALReportURL];
    NSMutableURLRequest *urlReq = [[NSMutableURLRequest alloc] initWithURL:url];
    NSString *postString = [NSString stringWithFormat:@"type=%@&latitude=%f&longitude=%f", reportType, coord.latitude, coord.longitude];
    
    [urlReq setHTTPMethod:@"POST"];
    [urlReq setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    // call a function to parse returned result and display to user
    [NSURLConnection sendAsynchronousRequest:urlReq queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
             // at some future point: better error processing
             // 1. queue to send later?
             // 2. add a callback hook to keep trying?
             
             [self performSelectorOnMainThread:@selector(doReportResult:) withObject:@"Sorry, an error occurred! (Problem contacting server.) Please try again in a moment.\n\nIf you receive this message again, check to make sure that your device is not in Airplane Mode and that you have Internet data access." waitUntilDone:NO];
             //[self doReportResult:@"Sorry, an error occurred! (Problem contacting server.) Please try again in a moment.\n\nIf you receive this message again, check to make sure that your device is not in Airplane Mode and that you have Internet data access."];
         else {
             [self performSelectorOnMainThread:@selector(parseServerResultCode:) withObject:data waitUntilDone:NO];
             // have to do on main thread so UI can update...
             //[self parseServerResultCode:data];
         }
     }];
    
}

- (void) doReportResult:(NSString *)message
{
    [self displayResultMessage:message withReturnButton:YES];
}

- (void) doTemporaryResultDisplay:(NSString *)message
{
    [self displayResultMessage:message withReturnButton:NO];
}

- (void) displayResultMessage:(NSString *)message withReturnButton:(BOOL)buttonOn
{
    CGRect viewRect = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    UIView *newView = [[UIView alloc] initWithFrame:viewRect];
    newView.backgroundColor = [UIColor colorWithWhite:kSALReportResultBackgroundGray alpha:1.0];
    
    //set up text
    CGRect textFrame = CGRectMake(kSALReportResultsTextXMargin,
                                  0,
                                  viewRect.size.width - (2*kSALReportResultsTextXMargin),
                                  viewRect.size.height);
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:textFrame];
    messageLabel.text = message;
    messageLabel.font = [UIFont boldSystemFontOfSize:kSALReportResultsTextSize];
    messageLabel.numberOfLines = 0;
    messageLabel.textAlignment = NSTextAlignmentCenter;
    [messageLabel sizeToFit];
    textFrame = CGRectMake(viewRect.size.width / 2 - messageLabel.frame.size.width / 2,
                           viewRect.size.height / 2 - messageLabel.frame.size.height / 2,
                           messageLabel.frame.size.width,
                           messageLabel.frame.size.height);
    messageLabel.frame = textFrame;
    [newView addSubview:messageLabel];
    
    if (buttonOn) // set up "OK" button
    {
        UIButton *okButton = [UIButton buttonWithType:UIButtonTypeSystem];
        okButton.userInteractionEnabled = YES;
        [okButton setTitle:@"OK" forState:UIControlStateNormal];
        [okButton sizeToFit];
        
        okButton.layer.cornerRadius = 6;
        okButton.layer.borderWidth = 1;
        okButton.layer.borderColor = okButton.tintColor.CGColor;
        okButton.backgroundColor = [UIColor colorWithWhite:(kSALReportResultBackgroundGray + .05) alpha:1.0];
        
        textFrame.origin.y -= (okButton.frame.size.height + kSALReportResultsTextButtonBuffer) / 2;
        messageLabel.frame = textFrame;
        CGRect buttonFrame = okButton.frame;
        buttonFrame.size.width *= 2;
        buttonFrame.origin.x = viewRect.size.width/2 - buttonFrame.size.width/2;
        buttonFrame.origin.y = textFrame.origin.y + textFrame.size.height + kSALReportResultsTextButtonBuffer;
        okButton.frame = buttonFrame;
        [okButton addTarget:self action:@selector(resetToRoot) forControlEvents:UIControlEventTouchUpInside];
        [newView addSubview:okButton];
    }
    
    UIViewController *newVC = [[UIViewController alloc] init];
    newVC.view = newView;
    self.newlyCreatedViewController = newVC;
    //[self.navigationController pushViewController:newVC animated:YES];
    
    newVC.navigationItem.hidesBackButton = YES;
    //[newVC.navigationItem setHidesBackButton:YES];
    //[newVC.navigationItem setBackBarButtonItem:nil];
    //[newVC.navigationItem setLeftBarButtonItem:nil];
    
    [self safelyPushViewController:newVC];
    
    
}

- (void) parseServerResultCode:(NSData *)resultData
{
    NSString *resultCode = [[NSString alloc] initWithData:resultData encoding:NSUTF8StringEncoding];
    if (resultCode == nil) //if there's an error?
    {
        [self doReportResult:@"Error parsing server result code! In theory this should never happen. Maybe try again later?"];
        return;
    }
    
    BOOL resultOK = ([resultCode caseInsensitiveCompare:kSALReportResultOKCode] == NSOrderedSame);
    
    if (resultOK)
    {
        [self doReportResult:@"Report completed successfully! Thanks for helping out!"];
        
        // tell map view to update
        SALAppDelegate *sharedAppDelegate = [[UIApplication sharedApplication] delegate];
        [sharedAppDelegate.mapVC updateModelAnnotationsForce:YES];
    }
    else
        [self doReportResult:[NSString stringWithFormat:@"Sorry, an error occurred. Server result code: %@\n\nMaybe try again later?",resultCode]];
}

- (void) resetToRoot
{
    self.currentScreen = self.model.rootReportScreen;
    self.title = self.currentScreen.title;
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void) safelyPushViewController:(UIViewController *)vc
{
    if (self.safeToPushNewViewController)
    {
        self.safeToPushNewViewController = NO;
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
    
    [self performSelector:@selector(safelyPushViewController:) withObject:vc afterDelay:0.5];
}


#pragma mark - Navigation controller delegate methods

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
//    NSLog(@"Showing a new view controller...");
    if (viewController == self.newlyCreatedViewController)
        return;
    
    if (self.currentScreen.parent == nil)
        return;
    
    self.currentScreen = self.currentScreen.parent;
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    self.safeToPushNewViewController = YES;
}


#pragma mark - Data source & delegate protocol methods for table view

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//    NSLog(@"Returning number of table view sections (1)...");
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    NSLog(@"Returning number of table view rows: %d", [self.currentScreen.childrenTitles count]);
    return [self.currentScreen.childrenTitles count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSLog(@"Getting table view cell...");
    static NSString *cellID = @"SALReportViewTableCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:cellID];
    }
    cell.textLabel.text = [self.currentScreen.childrenTitles objectAtIndex:indexPath.row];
    
    id cellChild = [self.currentScreen.children objectAtIndex:indexPath.row];
    if ([cellChild isKindOfClass:[NSString class]])
        cell.accessoryType = UITableViewCellAccessoryNone;
    else
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//    NSLog(@"Creating cell with text: %@", cell.textLabel.text);
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    id modelChild = [self.currentScreen.children objectAtIndex:indexPath.row];
    
    if ([modelChild isKindOfClass:[NSString class]]) // terminal node
    {
        [self sendReportToServer:modelChild];
        return;
        // do more later, i.e. display "sent" screen with a button to return to root view?
    }
    
    self.currentScreen = modelChild;
    UITableViewController *newTableViewController = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    newTableViewController.title = self.currentScreen.title;
    newTableViewController.tableView.delegate = self;
    newTableViewController.tableView.dataSource = self;
    [newTableViewController.tableView reloadData];
    //[self.navigationController pushViewController:newTableViewController animated:YES];
    
    self.newlyCreatedViewController = newTableViewController;
    [self safelyPushViewController:newTableViewController];
    
    //[tableView deselectRowAtIndexPath:indexPath animated:NO];
    //BATTrailsViewController *trailsController = [[BATTrailsViewController alloc] initWithStyle:UITableViewStylePlain];
    //trailsController.selectedRegion = [regions objectAtIndex:indexPath.row];
    //[[self navigationController] pushViewController:trailsController animated:YES];
}



@end
