//
//  SALLocationFinder.m
//  Salus
//
//  Created by Matthew Johnson on 6/8/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import "SALLocationFinder.h"
#import "SALEmergencyViewModel.h"
#import "SALMapViewController.h"
#import "SALResourceViewModel.h"

@implementation SALLocationFinder


- (id)init //designated initializer
{
    self = [super init];
    if (self)
    {
        [self checkAuthorizationAndInitStuff];
    }
    return self;
}


- (void)updateEverybody
{
    NSLog(@"updateEverybody");
    [self updateEmergencyModel];
    [self updateResourceModel];
    [self updateMapVC];
}


- (void)updateEmergencyModel
{
    NSLog(@"updateEmergencyModel");
    if (self.emergencyModel != nil)
        [self.emergencyModel updateCountryCode:[self.currentLocPlacemark ISOcountryCode]];
}


- (void)updateResourceModel
{
    NSLog(@"updateResourceModel");
    if (self.resourceModel != nil)
        [self.resourceModel updateWithLocation:self.currentLocation];
}


- (void)updateMapVC
{
    NSLog(@"updateMapVC");
    if (self.mapVC != nil)
        [self.mapVC enableLocationDisplay];
}


- (void)startUpdating
{
    NSLog(@"startUpdating");
    if (self.locationManager == nil) // this should never happen, but just to be safe... (actually -- it might, if we let other classes call it)
        return;
    
//    if ([CLLocationManager significantLocationChangeMonitoringAvailable])
    if (NO)
        [self.locationManager startMonitoringSignificantLocationChanges];
    else
    {
        [self.locationManager startUpdatingLocation];
        self.doingHighEnergyUpdating = YES;
    }
}


- (void)startLeisurelyUpdating
{
    NSLog(@"startLeisurelyUpdating");
    if (self.locationManager == nil) // this should never happen, but just to be safe...
        return;
    
    if (self.doingHighEnergyUpdating)
    {
        [self.locationManager stopUpdatingLocation];
        self.doingHighEnergyUpdating = NO;
    }
    
    if ([CLLocationManager significantLocationChangeMonitoringAvailable])
        //[self.locationManager startMonitoringSignificantLocationChanges];
        NSLog(@"Temporarily removing startMonitoringSignificantLocationChanges to test regions");
    
    [self resetUpdateRegion];
}


- (void)resetUpdateRegion
{
    NSLog(@"resetUpdateRegion");
//    if (self.currentUpdateRegion != nil)
//        [self.locationManager stopMonitoringForRegion:self.currentUpdateRegion];
    [self stopRegionMonitoring];
    
    CLCircularRegion *updateRegion = [[CLCircularRegion alloc] initWithCenter:self.currentLocation.coordinate radius:kSALLocationFinderUpdateRadiusMeters identifier:@"Update region"];
    [self.locationManager startMonitoringForRegion:updateRegion];
}


- (void)stopRegionMonitoring
{
    NSLog(@"stopRegionMonitoring");
    
    if (self.locationManager == nil)
        return;
    
    NSSet *regs = [self.locationManager monitoredRegions];
    if (regs == nil || [regs count] == 0)
        return;
    
    CLRegion *curRegion;
    if ([regs count] == 1)
    {
        curRegion = [regs anyObject];
        [self.locationManager stopMonitoringForRegion:curRegion];
        return;
    }
    
    NSArray *regsArray = [regs allObjects];
    for (NSUInteger i = 0; i < [regsArray count]; i++)
    {
        curRegion = [regsArray objectAtIndex:i];
        [self.locationManager stopMonitoringForRegion:curRegion];
    }
}


- (void)shutDown
{
    NSLog(@"shutDown");
    self.locationManager = nil;
    self.geocoder = nil;
    [self.forceUpdateTimer invalidate];
    self.forceUpdateTimer = nil;
    self.haveInitialFix = NO;
    self.doingHighEnergyUpdating = NO;
}


- (void)forceLocationUpdate
{
    NSLog(@"forceLocationUpdate");
    [self shutDown];
    [self checkAuthorizationAndInitStuff];
}


- (void)resetLocationUpdateTimer
{
    NSLog(@"resetLocationUpdateTimer");
    if (self.forceUpdateTimer)
        [self.forceUpdateTimer invalidate];
    self.forceUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:kSALLocationFinderForceLocationUpdateTime target:self selector:@selector(forceLocationUpdate) userInfo:nil repeats:YES];
}


- (void)checkAuthorizationAndInitStuff
{
    NSLog(@"checkAuthorizationAndInitStuff");
    
    if (![CLLocationManager locationServicesEnabled])
    {
        [self shutDown];
        [self displayLocationTurnOnRequest];
        return;
    }
    
    CLAuthorizationStatus authStatus = [CLLocationManager authorizationStatus];
    
    if (authStatus == kCLAuthorizationStatusNotDetermined)
    {
        NSLog(@"iOS 8 placeholder");
        // something involving [self.locationManager requestWhenInUseAuthorization] when we get to iOS 8 compatibility
        
    }
    
    if (authStatus == kCLAuthorizationStatusRestricted || authStatus == kCLAuthorizationStatusRestricted)
    {
        [self shutDown];
        [self displayLocationPermissionRequest];
        return;
    }
    
    // assuming we get to here, supposedly we can assume we are authorized and don't have to explicitly check for
    //  kCLAuthorizationStatusAuthorized (iOS 7 and below) or
    //  kCLAuthorizationStatusAuthorizedAlways / kCLAuthorizationStatusAuthorizedWhenInUse (iOS 8+)
    
    // (or, alternately, if we're on iOS 7 and our status is not determined, we just have to try location services and see if we get an error
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.geocoder = [[CLGeocoder alloc] init];
    [self resetLocationUpdateTimer];
    [self startUpdating];
}


- (void)displayLocationPermissionRequest
{
    //NSLog(@"Hey! Give me permission to track location, dummy!");
    UIAlertView *locationPermissionReq = [[UIAlertView alloc] initWithTitle:@"Permission to use location?"
                                                                    message:kSALLocationFinderLocationPermissionDeniedMessage
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
    [locationPermissionReq show];
}

- (void)displayLocationTurnOnRequest
{
    UIAlertView *locationTurnOnReq = [[UIAlertView alloc] initWithTitle:@"Turn on location services?"
                                                                message:kSALLocationFinderLocationServicesTurnedOffMessage
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
    [locationTurnOnReq show];
}


#pragma mark - CLLocationManagerDelegate methods

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"locationManager:didUpdateLocations:");
    
    if (locations == nil)
        return;
    
    self.currentLocation = [locations lastObject]; //lastObject is better than first; ordered by arrival so last is most recent
    [self.geocoder reverseGeocodeLocation:self.currentLocation
                        completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (placemarks == nil)
         {
             if (error != nil && error.code == kCLErrorNetwork) //if we had a network error, try again in 30 seconds...
             {
                 [self shutDown];
                 [self performSelector:@selector(checkAuthorizationAndInitStuff) withObject:nil afterDelay:kSALLocationFinderRetryTime];
             }
             return;
         }
         
         self.currentLocPlacemark = [placemarks objectAtIndex:0];
         if (self.haveInitialFix == NO || self.doingHighEnergyUpdating == YES)
         {
             self.haveInitialFix = YES;
             [self startLeisurelyUpdating];
         }
         else
         {
             [self resetUpdateRegion];
         }
         //NSLog(@"Current country: %@", [self.currentLocPlacemark country]);
         //NSLog(@"Current country code: %@", [self.currentLocPlacemark ISOcountryCode]);
         [self resetLocationUpdateTimer];
         [self updateEverybody];
     }];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"locationManager:didFailWithError:");
    
    // for the moment, mainly check for authorization errors
    if (error.code == kCLErrorDenied && manager == self.locationManager)
    {
        [self shutDown];
        [self displayLocationPermissionRequest];
        return;
    }
    
    //if anything else, try waiting and retrying?
    [self shutDown];
    [self performSelector:@selector(checkAuthorizationAndInitStuff) withObject:nil afterDelay:kSALLocationFinderRetryTime];
}

- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error
{
    NSLog(@"locationManager:monitoringDidFailForRegion:withError:");
    
    //for the moment, don't do anything special -- just do what we do for other errors and retry after a delay
    [self locationManager:manager didFailWithError:error];
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    NSLog(@"locationManager:didExitRegion:");
    
    //[manager stopMonitoringForRegion:region];
    //self.currentUpdateRegion = nil;
    [self stopRegionMonitoring];
    [self startUpdating];
}


#pragma mark - Convenience methods

+ (CLLocationDistance) distanceFromCLLocation:(CLLocation *)clloc toLat:(CLLocationDegrees)latitude andLong:(CLLocationDegrees)longitude
{
    CLLocation *otherLoc = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    CLLocationDistance locDist = [clloc distanceFromLocation:otherLoc]; //mostly for debugging so we can inspect the output
    return locDist;
}

+ (CLLocationDistance) distanceBetweenCLLocCoord:(CLLocationCoordinate2D)coord1 andCLLocCoord:(CLLocationCoordinate2D)coord2
{
    CLLocation *loc1 = [[CLLocation alloc] initWithLatitude:coord1.latitude longitude:coord1.longitude];
    return [SALLocationFinder distanceFromCLLocation:loc1 toLat:coord2.latitude andLong:coord2.longitude];
}

+ (void) findMostLikelyCoordForAddressStrArray:(NSArray *)addressStrs andCoord:(CLLocationCoordinate2D)coord withDistance:(CLLocationDistance)meters callbackObj:(id)obj andSelector:(SEL)sel;
{
    // for when each line of your address is in a separate string; this just joins them for convenience
    // the callback object/selector are expected to take one argument, a CLPlacemark *
    
    NSString *address = [addressStrs componentsJoinedByString:@", "];
    [SALLocationFinder findMostLikelyCoordForAddress:address andCoord:coord withDistance:meters callbackObj:obj andSelector:sel];
}

+ (void) findMostLikelyCoordForAddress:(NSString *)address andCoord:(CLLocationCoordinate2D)coord withDistance:(CLLocationDistance)meters callbackObj:(id)obj andSelector:(SEL)sel
{
    // the callback object/selector are expected to take one argument, a CLPlacemark *
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    CLLocationDistance searchRadius = meters*kSALLocationFinderAddressSearchRadiusMultiplier;
    
    CLCircularRegion *searchRegion = [[CLCircularRegion alloc] initWithCenter:coord radius:searchRadius identifier:@"temporarySearchRegion" ];
    [geocoder geocodeAddressString:address
                          inRegion:searchRegion
                 completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (placemarks == nil) //just return nil; will let the calling function decide if they want to retry
             [obj performSelectorOnMainThread:sel withObject:nil waitUntilDone:NO];
         
         CLPlacemark *returnPlacemark;
         if ([placemarks count] > 1)
             returnPlacemark = [SALLocationFinder findBestMatchPlacemarkToCoord:coord fromPlacemarks:placemarks withDistance:meters];
         else
             returnPlacemark = [placemarks objectAtIndex:0];
         
         [obj performSelectorOnMainThread:sel withObject:returnPlacemark waitUntilDone:NO];
     }];
}

+ (CLPlacemark *) findBestMatchPlacemarkToCoord:(CLLocationCoordinate2D)coord fromPlacemarks:(NSArray *)placemarks withDistance:(CLLocationDistance)meters
{
    NSUInteger nearestMatchInd = 0;
    CLPlacemark *tempPlacemark = [placemarks objectAtIndex:0];
    CLLocation *tempLocation = tempPlacemark.location;
    CLLocationDistance tempDistance = [SALLocationFinder distanceFromCLLocation:tempLocation
                                                                          toLat:coord.latitude
                                                                        andLong:coord.longitude];
    CLLocationDistance nearestMatchDistDiff = fabs(tempDistance - meters);
    
    CLLocationDistance tempDistDiff;
    for (NSUInteger i = 1; i<[placemarks count]; i++)
    {
        tempPlacemark = [placemarks objectAtIndex:i];
        tempLocation = tempPlacemark.location;
        tempDistance = [SALLocationFinder distanceFromCLLocation:tempLocation
                                                           toLat:coord.latitude
                                                         andLong:coord.longitude];
        tempDistDiff = fabs(tempDistance - meters);
        if (tempDistDiff < nearestMatchDistDiff)
        {
            nearestMatchDistDiff = tempDistDiff;
            nearestMatchInd = i;
        }
    }
    
    tempPlacemark = [placemarks objectAtIndex:nearestMatchInd];
    return tempPlacemark;
}


# pragma mark - convert ALL the data types!

+ (CLLocationCoordinate2D) midpointBetweenCoord:(CLLocationCoordinate2D)coord1 andCoord:(CLLocationCoordinate2D)coord2
{
    // for now, just do it the dumb way -- don't worry about curvature of Earth or the 180th meridian
    CLLocationCoordinate2D midpoint;
    midpoint.latitude = (coord1.latitude + coord2.latitude) / 2;
    midpoint.longitude = (coord1.longitude + coord2.longitude) / 2;
    return midpoint;
}

+ (CLLocationDistance) latDistanceMetersBetweenCoord:(CLLocationCoordinate2D)coord1 andCoord:(CLLocationCoordinate2D)coord2
{
    // again, let's keep it stupid -- assume small areas where curvature doesn't matter
    CLLocationDegrees degDiff = fabs(coord1.latitude - coord2.latitude);
    
    // we'll use the midpoint longitude, though it shouldn't matter
    CLLocationCoordinate2D midpoint = [SALLocationFinder midpointBetweenCoord:coord1 andCoord:coord2];
    CLLocationCoordinate2D temp1 = midpoint;
    CLLocationCoordinate2D temp2 = midpoint;
    temp1.latitude += (degDiff/2);
    temp2.latitude -= (degDiff/2);
    return [SALLocationFinder distanceBetweenCLLocCoord:temp1 andCLLocCoord:temp2];
}


+ (CLLocationDistance) longDistanceMetersBetweenCoord:(CLLocationCoordinate2D)coord1 andCoord:(CLLocationCoordinate2D)coord2
{
    // again, let's keep it stupid -- assume small areas where curvature doesn't matter
    CLLocationDegrees degDiff = fabs(coord1.longitude - coord2.longitude);
    
    // we'll use the midpoint latitude -- better than nothing
    CLLocationCoordinate2D midpoint = [SALLocationFinder midpointBetweenCoord:coord1 andCoord:coord2];
    CLLocationCoordinate2D temp1 = midpoint;
    CLLocationCoordinate2D temp2 = midpoint;
    temp1.longitude += (degDiff/2);
    temp2.longitude -= (degDiff/2);
    return [SALLocationFinder distanceBetweenCLLocCoord:temp1 andCLLocCoord:temp2];
}


+ (CLLocationCoordinate2D) minLatLongOfCoord:(CLLocationCoordinate2D)coord1 andCoord:(CLLocationCoordinate2D)coord2
{
    // returns a single point that has the minimum latitude and longitude of the two input points
    //  i.e., the southwest corner of the map containing them (in degrees)
    
    CLLocationCoordinate2D coord;
    if (coord1.latitude < coord2.latitude)
        coord.latitude = coord1.latitude;
    else
        coord.latitude = coord2.latitude;
    
    if (coord1.longitude < coord2.longitude)
        coord.longitude = coord1.longitude;
    else
        coord.longitude = coord2.longitude;
    
    return coord;
}


+ (CLLocationCoordinate2D) maxLatLongOfCoord:(CLLocationCoordinate2D)coord1 andCoord:(CLLocationCoordinate2D)coord2
{
    // returns a single point that has the maximum latitude and longitude of the two input points
    //  i.e., the northeast corner of the map containing them (in degrees)
    
    CLLocationCoordinate2D coord;
    if (coord1.latitude > coord2.latitude)
        coord.latitude = coord1.latitude;
    else
        coord.latitude = coord2.latitude;
    
    if (coord1.longitude > coord2.longitude)
        coord.longitude = coord1.longitude;
    else
        coord.longitude = coord2.longitude;
    
    return coord;
}



@end
