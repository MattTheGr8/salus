//
//  SALUtilFuncs.h
//  Salus
//
//  Created by Matthew Johnson on 10/16/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SALUtilFuncs : NSObject

    + (void) loadImageAsyncWithURL:(NSString *)urlStr andCallbackObj:(id)obj andSelector:(SEL)sel;
    + (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
    + (UIImage *)imageWithImage:(UIImage *)image scaledToWidth:(CGFloat)newWidth;

    // Yelp-related utilities
    + (BOOL) isYelpInstalled;
    + (void) openYelpURL:(NSString *)urlStr;
@end
