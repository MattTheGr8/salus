//
//  SALCopyableUILabel.m
//  Salus
//
//  Created by Matthew Johnson on 10/25/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import "SALCopyableUILabel.h"

@implementation SALCopyableUILabel

// everything taken straight from https://github.com/zoul/UILabel-Clipboard-Sample/blob/master/Classes/CopyLabel.m
// - just changed the name of the class
// - actually, also fixed a couple of things -- ARC updating, compiler complaints
// - also had to get the actual copy method from http://stackoverflow.com/questions/1246198/show-iphone-cut-copy-paste-menu-on-uilabel

#pragma mark - Initialization

- (void) attachTapHandler
{
    [self setUserInteractionEnabled:YES];
    UIGestureRecognizer *touchy = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self action:@selector(handleTap:)];
    [self addGestureRecognizer:touchy];
}

- (id) initWithFrame: (CGRect) frame
{
    self = [super initWithFrame:frame];
    [self attachTapHandler];
    return self;
}

- (void) awakeFromNib
{
    [super awakeFromNib];
    [self attachTapHandler];
}

#pragma mark - Clipboard

- (void) copy:(id)sender {
    UIPasteboard *pboard = [UIPasteboard generalPasteboard];
    pboard.string = self.text;
}

- (BOOL) canPerformAction: (SEL) action withSender: (id) sender
{
    return (action == @selector(copy:));
}

- (void) handleTap: (UIGestureRecognizer*) recognizer
{
    [self becomeFirstResponder];
    UIMenuController *menu = [UIMenuController sharedMenuController];
    [menu setTargetRect:self.frame inView:self.superview];
    [menu setMenuVisible:YES animated:YES];
}

- (BOOL) canBecomeFirstResponder
{
    return YES;
}


@end
