//
//  SALResourceViewModelItem.h
//  Salus
//
//  Created by Matthew Johnson on 10/4/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

#define kSALResourceViewItemCategoryPharmacy            1
#define kSALResourceViewItemCategoryDentist             2
#define kSALResourceViewItemCategoryDoctor              3
#define kSALResourceViewItemCategoryChiro               4
#define kSALResourceViewItemCategoryCounseling          5
#define kSALResourceViewItemCategoryER                  6
#define kSALResourceViewItemCategoryHospital            7
#define kSALResourceViewItemCategoryMidwife             8
#define kSALResourceViewItemCategoryOptom               9
#define kSALResourceViewItemCategoryRehab               10
#define kSALResourceViewItemCategoryUCare               11

#define kSALResourceViewItemCategoryPharmacyStr         @"Pharmacy"
#define kSALResourceViewItemCategoryDentistStr          @"Dentist"
#define kSALResourceViewItemCategoryDoctorStr           @"Doctor"
#define kSALResourceViewItemCategoryChiroStr            @"Chiropractor"
#define kSALResourceViewItemCategoryCounselingStr       @"Counseling/Mental Health"
#define kSALResourceViewItemCategoryERStr               @"Emergency Room"
#define kSALResourceViewItemCategoryHospitalStr         @"Hospital"
#define kSALResourceViewItemCategoryMidwifeStr          @"Midwife"
#define kSALResourceViewItemCategoryOptomStr            @"Optometrist"
#define kSALResourceViewItemCategoryRehabStr            @"Rehabilitation Center"
#define kSALResourceViewItemCategoryUCareStr            @"Urgent Care"

#define kSALResourceViewItemCategoryPharmacyServerStr   @"pharmacy"
#define kSALResourceViewItemCategoryDentistServerStr    @"dentists"
#define kSALResourceViewItemCategoryDoctorServerStr     @"physicians"
#define kSALResourceViewItemCategoryChiroServerStr      @"chiropractors"
#define kSALResourceViewItemCategoryCounselingServerStr @"c_and_mh"
#define kSALResourceViewItemCategoryERServerStr         @"emergencyrooms"
#define kSALResourceViewItemCategoryHospitalServerStr   @"hospitals"
#define kSALResourceViewItemCategoryMidwifeServerStr    @"midwives"
#define kSALResourceViewItemCategoryOptomServerStr      @"optometrists"
#define kSALResourceViewItemCategoryRehabServerStr      @"rehabilitation_center"
#define kSALResourceViewItemCategoryUCareServerStr      @"urgent_care"


#define kSALResourceViewItemCategoryKey                 @"category"
#define kSALResourceViewItemNameKey                     @"name"
#define kSALResourceViewItemDisplayPhoneKey             @"display_phone"
#define kSALResourceViewItemPhoneKey                    @"phone"
#define kSALResourceViewItemDistanceKey                 @"distance_m"
#define kSALResourceViewItemImageURLKey                 @"image"
#define kSALResourceViewItemAddressKey                  @"address"
#define kSALResourceViewItemYelpLinkKey                 @"yelp_link"
#define kSALResourceViewItemRatingImageURLKey           @"rating"
#define kSALResourceViewItemReviewCountKey              @"review_count"

#define mSALResourceViewReturnNilIfTempValNil           if (tempVal == nil) return nil;


@interface SALResourceViewModelItem : NSObject

    @property (assign) NSUInteger category;
    @property (strong) NSString *name;
    @property (strong) NSString *displayPhone;
    @property (strong) NSString *phoneNumber;
    @property (assign) float distanceMeters;
    @property (strong) NSString *imageURL;
    @property (strong) NSArray *addressStrArray;
    @property (strong) NSString *yelpLink;
    @property (strong) NSString *ratingImageURL;
    @property (assign) NSUInteger reviewCount;
    @property (assign) CLLocationCoordinate2D requestCoord;

    + (SALResourceViewModelItem *) resourceItemFromDictionary:(NSDictionary *)dict;
    + (id) getCheckItemFromDictionary:(NSDictionary *)dict withKey:(NSString *)key andClass:(id)aClass;
    + (NSUInteger) categoryCodeForServerCategoryString:(NSString *)catStr;
    + (NSString *) categoryNameForCategoryCode:(NSUInteger)catCode;
    - (NSString *) categoryStr;
    - (NSString *) distanceStrLocalized;
    - (NSString *) reviewCountStr;
    - (NSComparisonResult)compareDistanceToOtherItem:(SALResourceViewModelItem *)other;
    - (BOOL) hasValidImageURL; // convenience method

@end
