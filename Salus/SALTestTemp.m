//
//  SALTestTemp.m
//  Salus
//
//  Created by Matthew Johnson on 10/12/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import "SALTestTemp.h"

@implementation SALTestTemp

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
