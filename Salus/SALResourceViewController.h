//
//  SALResourceViewController.h
//  Salus
//
//  Created by Matthew Johnson on 9/22/14.
//  Copyright (c) 2014 Matthew Johnson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@class SALResourceViewModel;
@class SALResourceTableViewController;
@class SALResourceSubViewController;
@class SALResourceViewModelItem;

@interface SALResourceViewController : UINavigationController <UINavigationControllerDelegate>

    @property (strong) SALResourceViewModel *model;
    @property (strong) SALResourceTableViewController *tableVC;
    @property (strong) SALResourceSubViewController *subVC;
    @property (strong) UIViewController *currentVC;
    @property (assign) BOOL safeToPushNewViewController;

    - (void) safelyPushViewController:(UIViewController *)vc;
    - (void) createPushSubViewControllerForModelItem:(SALResourceViewModelItem *)modelItem withImage:(UIImage *)img;
    //- (void) updateWithCoordinate:(CLLocationCoordinate2D)coord;
    - (void) updateFromModel;

    // Navigation controller delegate methods
    - (void) navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated;


@end
